package types

// These are constants for context error msg used in cacao service error,
// e.g. service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError)
const (
	GeneralActorNotAuthorizedError     string = "actor not authorized"
	GeneralActorNotFoundError          string = "actor was not found"
	UserUsernameNotFoundError          string = "username not found"
	UserUsernameReservedCannotAddError string = "username is reserved, cannot add user"
	UserUsernameExistsCannotAddError   string = "username exists, cannot add user"
)
