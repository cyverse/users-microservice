package types

import (
	"context"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// Specification - holds the configurable constants for the microservice
type Specification struct {
	DbURI        string          `default:"mongodb://localhost:27017" split_words:"true"`
	DbName       string          `default:"users" split_words:"true"`
	ClusterID    string          `default:"cacao-cluster" split_words:"true"`
	NatsURL      string          `default:"nats://nats:4222" split_words:"true"`
	NatsClientID string          `envconfig:"NATS_CLIENT_ID" default:"users-microservice"`
	AppContext   context.Context `ignored:"true"`
	LogLevel     string          `default:"debug" split_words:"true"`
}

// NatsConfigFromSpec ...
func NatsConfigFromSpec(s Specification) messaging2.NatsConfig {
	return messaging2.NatsConfig{
		URL:             s.NatsURL,
		QueueGroup:      defaultNatsQGroup,
		WildcardSubject: defaultNatsWildcardSubject,
		ClientID:        s.NatsClientID,
		MaxReconnects:   defaultNatsMaxReconnect,
		ReconnectWait:   defaultNatsReconnectWait,
		RequestTimeout:  0,
	}
}

// StanConfigFromSpec ...
func StanConfigFromSpec(s Specification) (messaging2.NatsConfig, messaging2.StanConfig) {
	return NatsConfigFromSpec(s), messaging2.StanConfig{
		ClusterID:   s.ClusterID,
		DurableName: defaultNatsDurableName,
	}
}

// MessagingConfigFromSpec ...
func MessagingConfigFromSpec(s Specification) messaging2.NatsStanMsgConfig {
	natsConf, stanConf := StanConfigFromSpec(s)
	return messaging2.NatsStanMsgConfig{
		NatsConfig: natsConf,
		StanConfig: stanConf,
	}
}
