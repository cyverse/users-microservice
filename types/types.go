// Package types contains shared types across the entire microservice
package types

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// UserOp contains the data to submit a query to the domain
type UserOp struct {
	User          *DomainUserModel     // contains the username of the person of interest, including the SessionActor
	Op            string               // this is the operation; in this case this maps to the subject
	TransactionID common.TransactionID // this ID is used to keep track of transactions, pairing request event and response event (e.g. UserAddRequested & UserAdded) should have the same TransactionID
	ReplyChan     chan DomainUserModel // this is the channel used for a reply; for a single user, only the first array element is populated
}

// GetOp ...
// Note this method is implemented to access Op field in generics function.
// Since Go does not support access struct field in generics yet, https://github.com/golang/go/issues/48522
func (op UserOp) GetOp() string { return op.Op }

// UserSettingsOp contains the data to submit a query to the domain
type UserSettingsOp struct {
	Username      string
	Key           string
	Value         string
	Op            string               // this is the operation; in this case this maps to the subject
	TransactionID common.TransactionID // this ID is used to keep track of transactions, pairing request event and response event (e.g. UserAddRequested & UserAdded) should have the same TransactionID
	ReplyChan     chan common.UserSettings
}

// GetOp ...
// Note this method is implemented to access Op field in generics function.
// Since Go does not support access struct field in generics yet, https://github.com/golang/go/issues/48522
func (op UserSettingsOp) GetOp() string { return op.Op }

// UserListQuery contains the data to submit a query to the domain
// Note, since we're not directly storing the UserList, we can leverage
// the service.UserListModel directly without declaring a domain-specific UserListModel
type UserListQuery struct {
	UserList  *service.UserListModel // contains the username of the person of interest, including the SessionActor
	Op        string                 // this is the operation; in this case this maps to the subject
	ReplyChan chan []DomainUserModel // this is the channel used for a reply; for a user list
}

// GetOp ...
// Note this method is implemented to access Op field in generics function.
// Since Go does not support access struct field in generics yet, https://github.com/golang/go/issues/48522
func (op UserListQuery) GetOp() string { return op.Op }

// DomainUserModel is the domain-specific struct to represent the user. This is a separate entity to protect against
// potential drift that might happen from UserModel found in the cacao-types/types. In practice, this should model
// should not drift too much. It is not recommended exporting this struct outside of
// the domain except for debugging purposes, if at all.
// for convenience, we can set the bson tags for more convenient conversion
type DomainUserModel struct {
	Username          string       `bson:"_id"` // for this object, the username is the primary id
	FirstName         string       `bson:"first_name"`
	LastName          string       `bson:"last_name"`
	PrimaryEmail      string       `bson:"primary_email"`
	IsAdmin           bool         `bson:"is_admin"`
	DisabledAt        time.Time    `bson:"disabled_at"`
	UserSettings      UserSettings `bson:"-"` // settings are stored separately than the user object
	CreatedAt         time.Time    `bson:"created_at"`
	UpdatedAt         time.Time    `bson:"updated_at"`
	UpdatedBy         string       `bson:"updated_by"`
	UpdatedEmulatorBy string       `bson:"updated_emulator_by"` // only populatated if emulator !- actor
	SessionActor      string       `bson:"-"`
	SessionEmulator   string       `bson:"-"`
	ErrorType         string       `bson:"-"`
	ErrorMessage      string       `bson:"-"`
}

// UserSettings ...
type UserSettings struct {
	Configs   map[string]string   `bson:"-"`
	Favorites map[string][]string `bson:"-"`
	Recents   map[string]string   `bson:"-"`
}

// NewDomainUserModel constructor, will not populate CreatedAt, UpdatedAt, and UpdatedEmulatorBy
func NewDomainUserModel(user service.UserModel) *DomainUserModel {
	newuser := DomainUserModel{
		Username:        user.Username,
		FirstName:       user.FirstName,
		LastName:        user.LastName,
		PrimaryEmail:    user.PrimaryEmail,
		IsAdmin:         user.IsAdmin,
		DisabledAt:      user.DisabledAt,
		SessionActor:    user.SessionActor,
		SessionEmulator: user.SessionEmulator,
	}
	return &newuser
}

// ConvertToServiceUserModel will create a service.UserModel from the DomainUserModel. In order to do this,
// the service.UserModel may need the actor and emulator
func (duser DomainUserModel) ConvertToServiceUserModel(actor string, emulator string) *service.UserModel {
	return &service.UserModel{
		Session: service.Session{SessionActor: actor, SessionEmulator: emulator,
			ErrorType: duser.ErrorType, ErrorMessage: duser.ErrorMessage},
		Username:          duser.Username,
		FirstName:         duser.FirstName,
		LastName:          duser.LastName,
		PrimaryEmail:      duser.PrimaryEmail,
		IsAdmin:           duser.IsAdmin,
		CreatedAt:         duser.CreatedAt,
		UpdatedAt:         duser.UpdatedAt,
		UpdatedBy:         duser.UpdatedBy,
		UpdatedEmulatorBy: duser.UpdatedEmulatorBy,
		DisabledAt:        duser.DisabledAt,
		UserSettings: common.UserSettings{
			Configs:   duser.UserSettings.Configs,
			Favorites: duser.UserSettings.Favorites,
			Recents:   duser.UserSettings.Recents,
		},
	}
}

// SettingsEvent is the body of outgoing events for updated settings
type SettingsEvent struct {
	Username string `json:"username"`
	Key      string `json:"key"`
	Value    string `json:"value"`
}

// NewSettingsEvent ...
func NewSettingsEvent(username, key, value string) SettingsEvent {
	return SettingsEvent{Username: username, Key: key, Value: value}
}
