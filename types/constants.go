package types

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"time"

	"github.com/nats-io/nats.go"
)

// TODO: DEVELOPER should changes these e.g. instead of "mythical*", change to "user*" for user microservice, "credentials*", "workspaces*", etc
const (
	defaultNatsMaxReconnect  = 6  // max times to reconnect within nats.connect()
	defaultNatsReconnectWait = 10 // seconds to wait within nats.connect()

	DefaultNatsClientID        = "users-microservice"
	defaultNatsQGroup          = "users_qgroup"
	defaultNatsDurableName     = "users_durable"
	DefaultNatsEventsSubject   = common.EventsSubject
	defaultNatsWildcardSubject = "cyverse.users.*"

	DefaultNatsCoreTimeout = 24 * 60 * 60 * time.Second // timeout before wiretap exipires, currently set so that 1 day is a-ok
)

// ClientID - client id for nats streaming
const ClientID string = "users-microservice"

// ClientIDResponderExt is added to end of client id when responding on the events queue
const ClientIDResponderExt string = "-responder"

// EventsSubject - nats streaming queue subject for events
const EventsSubject string = "cyverse.events"

// EventsURLDefault - default nats streaming URL for events, used when EventsURL is not specified in Specification
const EventsURLDefault string = nats.DefaultURL

// QueriesURLDefault - default nats streaming URL for queries, used when QueriesURL is not specified in Specification
const QueriesURLDefault string = nats.DefaultURL

// UsersGetPropertySubject - subject for nats subscription for UsersGetProperty requests
const UsersGetPropertySubject string = "cyverse.users.getProperty"

// DefaultQueryChannelBufferSize is the default size for channels for queries
const DefaultQueryChannelBufferSize = 100

// DefaultEventChannelBufferSize is the default size for channels for events
// The buffer size for events should not be too large, since we will want to process the events in the buffer before gracefully exit.
const DefaultEventChannelBufferSize = 3

// ConfigVarPrefix is the prefix used for environment variables
const ConfigVarPrefix string = "usersms"

// CloudEventSourceBaseValue is the source string value used for all outgoing cloudevent messages
const CloudEventSourceBaseValue string = "https://gitlab.com/cyverse/users-microservice/"

// EnvPodNameVar is used for determining unique source value
const EnvPodNameVar string = "POD_NAME"

// MongoDbUserCollection name of the mongo collection
const MongoDbUserCollection = "users"

// MongoDbUserSettingsCollection name of the mongo collection for user settings
const MongoDbUserSettingsCollection = "settings"

// MaxUserListSize is the maximum number of users to return in a list
// TODO: ultimately, change this to a reasonably larger number
const MaxUserListSize = 500 // small for now

// ReservedUsernameList is a list of reserved username that should not be used when creating user
var ReservedUsernameList = []string{service.ReservedCacaoSystemActor, service.ReservedAnonymousActor}

// Limits for user and settings schema, these are artificial limits to ensure we
// don't insert overly large object in database. They can be increased as needed.
const (
	MaxUsernameLength        = 128
	MaxFirstLastNameLength   = 128
	MaxEmailLength           = 128
	MaxSettingsKeyLength     = 128
	MaxSettingsValueLength   = 256
	MaxSettingsConfigPairs   = 100
	MaxSettingsFavoritePairs = 100
	MaxFavoriteItemsPerKey   = 50
	MaxSettingsRecentPairs   = 100
)
