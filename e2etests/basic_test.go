package e2etests

import (
	"context"
	"fmt"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"os"
	"strings"
	"sync"
	"sync/atomic"
	"testing"
	"time"
	"users-microservice/adapters"
	"users-microservice/domain"
	"users-microservice/ports"
	"users-microservice/types"
)

func newStorage(t *testing.T, s types.Specification) ports.PersistentStoragePort {
	storage := &adapters.MongoAdapter{}
	err := storage.Init(s)
	if err != nil {
		t.Fatal(err)
	}
	return storage
}

func cleanupStorage(t *testing.T, s types.Specification, storage ports.PersistentStoragePort) {
	adapter := storage.(*adapters.MongoAdapter)
	connection, err := db.NewMongoDBConnection(&db.MongoDBConfig{
		URL:    s.DbURI,
		DBName: s.DbName,
	})
	if err != nil {
		t.Error(err)
		return
	}
	err = connection.Database.Collection(types.MongoDbUserCollection).Drop(context.Background())
	if err != nil {
		t.Error(err)
		return
	}
	err = connection.Database.Collection(types.MongoDbUserSettingsCollection).Drop(context.Background())
	if err != nil {
		t.Error(err)
		return
	}
	log.Println("storage cleaned up")
	err = adapter.Close()
	if err != nil {
		t.Error(err)
		return
	}
}

type IntegrationTestFunc func(t *testing.T, s types.Specification)

func runIntegrationTest(t *testing.T, testFunc IntegrationTestFunc) {
	log.SetLevel(log.DebugLevel)
	if skipTestIfNoMongoDB(t) || skipTestIfNoNATS(t) {
		return
	}
	s := loadConfigFromEnv(t)
	svcCtx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()
	s.AppContext = svcCtx

	storage := newStorage(t, s)
	defer cleanupStorage(t, s, storage)

	natsConn, stanConn, err := createConnectionFromSpec(s)
	if !assert.NoError(t, err) {
		return
	}
	defer natsConn.Close()
	defer stanConn.Close()

	// create an initial Domain object
	ea := adapters.NewEventAdapter(stanConn)
	qa := adapters.QueryAdapter{}
	var dmain = domain.NewDomain(&qa, ea, storage)

	err = dmain.Init(s)
	if !assert.NoError(t, err) {
		cancelFunc()
		return
	}

	go dmain.Start(svcCtx)
	time.Sleep(time.Second) // wait for service to startup (connect to mongo and nats/stan)

	testFunc(t, s) // testing the service using service client from cacao-common/service
	cancelFunc()
	time.Sleep(time.Second * 2) // wait for service to shut down
}

// Test query and event operations supported by the service (get, add, update, delete).
// - Fetch user (not exist)
// - Add user
// - Fetch user (exist)
// - Update user
// - Fetch user after update
// - Delete user
// - Fetch user after delete (not exist)
//
// The service is spin up as inside a go routine, While the tests are running on the main thread.
// Note that due to some context issue, there cannot be another integration test that spin up the domain running in the same process (you can run them separately with `go test -run <TEST_NAME>`).
func TestGetAddGetUpdateGetDeleteGet(t *testing.T) {
	runIntegrationTest(t, testGetAddGetUpdateGetDeleteGet)
}

func testGetAddGetUpdateGetDeleteGet(t *testing.T, s types.Specification) {
	ctx, cancelSvcClient := context.WithTimeout(context.Background(), time.Second)
	defer cancelSvcClient()
	natsConn, stanConn, err := createConnectionFromSpec(s) // create separate connections for the service client
	if err != nil {
		return
	}
	defer natsConn.Close()
	defer stanConn.Close()
	svcClient, err := service.NewNatsUserClientFromConn(natsConn, stanConn)
	if !assert.NoError(t, err) {
		return
	}
	// create an admin user
	err = svcClient.Add(ctx, service.Actor{Actor: "RESERVED_CACAO_SYSTEM_ACTOR"}, service.UserModel{Username: "admin_user123", IsAdmin: true})
	if !assert.NoError(t, err) {
		return
	}

	// fetch non-existent
	fetchedUser, err := svcClient.Get(ctx, service.Actor{Actor: "RESERVED_CACAO_SYSTEM_ACTOR"}, "testuser123", false)
	if !assert.Errorf(t, err, "%v", fetchedUser) {
		return
	}
	if !assert.Nil(t, fetchedUser) {
		return
	}

	// add
	newUser := service.UserModel{Username: "testuser123"}
	err = svcClient.Add(ctx, service.Actor{Actor: "RESERVED_CACAO_SYSTEM_ACTOR"}, newUser)
	if !assert.NoError(t, err) {
		return
	}
	// re-add same user
	err = svcClient.Add(ctx, service.Actor{Actor: "RESERVED_CACAO_SYSTEM_ACTOR"}, newUser)
	if !assert.Error(t, err) {
		return
	}
	// this is the old way of checking if user is already created, and this no longer works
	assert.NotEqual(t, service.UserUsernameExistsCannotAddError, err.Error())
	// this is the new way of checking if user is already created
	assert.Equal(t, service.CacaoAlreadyExistErrorMessage, service.ToCacaoError(err).StandardError())

	// re-fetch
	fetchedUser, err = svcClient.Get(ctx, service.Actor{Actor: "RESERVED_CACAO_SYSTEM_ACTOR"}, "testuser123", false)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotNil(t, fetchedUser) {
		return
	}
	assert.Greater(t, time.Since(fetchedUser.CreatedAt), time.Duration(0))
	assert.Greater(t, time.Since(fetchedUser.UpdatedAt), time.Duration(0))
	fetchedUser.CreatedAt = time.Time{} // erase timestamp for easy comparison with assert
	fetchedUser.UpdatedAt = time.Time{} // erase timestamp for easy comparison with assert
	assert.Equal(t, &service.UserModel{
		Session: service.Session{
			SessionActor:    "RESERVED_CACAO_SYSTEM_ACTOR",
			SessionEmulator: "",
		},
		Username:          "testuser123",
		FirstName:         "",
		LastName:          "",
		PrimaryEmail:      "",
		IsAdmin:           false,
		DisabledAt:        time.Time{},
		WithSettings:      false,
		UserSettings:      common.UserSettings{},
		CreatedAt:         time.Time{},
		UpdatedAt:         time.Time{},
		UpdatedBy:         "RESERVED_CACAO_SYSTEM_ACTOR",
		UpdatedEmulatorBy: "",
	}, fetchedUser)

	// update
	err = svcClient.Update(ctx, service.Actor{Actor: "RESERVED_CACAO_SYSTEM_ACTOR"}, service.UserModel{
		Session:   service.Session{},
		Username:  "testuser123",
		FirstName: "foobar",
	})
	if !assert.Error(t, err) {
		return
	}
	{
		// system user is not authorized to update user
		e := err.(service.CacaoError)
		assert.Equal(t, service.CacaoUnauthorizedErrorMessage, e.StandardError())
	}

	err = svcClient.Update(ctx, service.Actor{Actor: "testuser123"}, service.UserModel{
		Session:           service.Session{},
		Username:          "testuser123",
		FirstName:         "foo",
		LastName:          "bar",
		PrimaryEmail:      "foobar@cyverse.org",
		IsAdmin:           false,
		DisabledAt:        time.Time{},
		WithSettings:      false,
		UserSettings:      common.UserSettings{},
		CreatedAt:         time.Time{},
		UpdatedAt:         time.Time{},
		UpdatedBy:         "",
		UpdatedEmulatorBy: "",
	})
	if !assert.NoError(t, err) {
		return
	}

	// fetch after update
	fetchedUser, err = svcClient.Get(ctx, service.Actor{Actor: "RESERVED_CACAO_SYSTEM_ACTOR"}, "testuser123", false)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotNil(t, fetchedUser) {
		return
	}
	assert.Greater(t, time.Since(fetchedUser.CreatedAt), time.Duration(0))
	assert.Greater(t, time.Since(fetchedUser.UpdatedAt), time.Duration(0))
	fetchedUser.CreatedAt = time.Time{} // erase timestamp for easy comparison with assert
	fetchedUser.UpdatedAt = time.Time{} // erase timestamp for easy comparison with assert
	assert.Equal(t, &service.UserModel{
		Session: service.Session{
			SessionActor:    "RESERVED_CACAO_SYSTEM_ACTOR",
			SessionEmulator: "",
		},
		Username:          "testuser123",
		FirstName:         "foo",
		LastName:          "bar",
		PrimaryEmail:      "foobar@cyverse.org",
		IsAdmin:           false,
		DisabledAt:        time.Time{},
		WithSettings:      false,
		UserSettings:      common.UserSettings{},
		CreatedAt:         time.Time{},
		UpdatedAt:         time.Time{},
		UpdatedBy:         "testuser123",
		UpdatedEmulatorBy: "",
	}, fetchedUser)

	// delete
	err = svcClient.Delete(context.TODO(), service.Actor{Actor: "testuser123"}, service.UserModel{Username: "testuser123"})
	if !assert.Error(t, err) {
		return
	}
	{
		// non-admin user is not authorized to delete self
		e := err.(service.CacaoError)
		assert.Equal(t, service.CacaoUnauthorizedErrorMessage, e.StandardError())
	}

	err = svcClient.Delete(context.TODO(), service.Actor{Actor: "admin_user123"}, service.UserModel{Username: "testuser123"})
	if !assert.NoError(t, err) {
		return
	}

	// fetch after delete
	fetchedUser, err = svcClient.Get(context.TODO(), service.Actor{Actor: "RESERVED_CACAO_SYSTEM_ACTOR"}, "testuser123", false)
	if !assert.Error(t, err) {
		return
	}
	if !assert.Nil(t, fetchedUser) {
		return
	}
}

func TestUserSettings(t *testing.T) {
	runIntegrationTest(t, testUserSettings)
}

func testUserSettings(t *testing.T, s types.Specification) {
	ctx, cancelSvcClient := context.WithTimeout(context.Background(), time.Second)
	defer cancelSvcClient()
	natsConn, stanConn, err := createConnectionFromSpec(s) // create separate connections for the service client
	if err != nil {
		return
	}
	defer natsConn.Close()
	defer stanConn.Close()
	svcClient, err := service.NewNatsUserClientFromConn(natsConn, stanConn)
	if !assert.NoError(t, err) {
		return
	}
	// create an admin user
	err = svcClient.Add(ctx, service.Actor{Actor: "RESERVED_CACAO_SYSTEM_ACTOR"}, service.UserModel{
		Session:           service.Session{},
		Username:          "admin_user123",
		FirstName:         "Foo",
		LastName:          "Bar",
		PrimaryEmail:      "FooBar@example.com",
		IsAdmin:           true,
		DisabledAt:        time.Time{},
		WithSettings:      false,
		UserSettings:      common.UserSettings{},
		CreatedAt:         time.Time{},
		UpdatedAt:         time.Time{},
		UpdatedBy:         "",
		UpdatedEmulatorBy: "",
	})
	if !assert.NoError(t, err) {
		return
	}
	// create a non-admin user
	err = svcClient.Add(ctx, service.Actor{Actor: "RESERVED_CACAO_SYSTEM_ACTOR"}, service.UserModel{
		Session:  service.Session{},
		Username: "nonadmin_user123",
		IsAdmin:  false,
	})
	if !assert.NoError(t, err) {
		return
	}

	adminActor := service.Actor{Actor: "admin_user123"}

	// get the user settings, should be all empty
	settings, err := svcClient.GetSettings(ctx, adminActor, adminActor.Actor)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotNil(t, settings) {
		return
	}
	assert.Lenf(t, settings.Configs, 0, "%+v", settings.Configs)
	assert.Len(t, settings.Favorites, 0)
	assert.Len(t, settings.Recents, 0)
	configs, err := svcClient.GetConfigs(ctx, adminActor, adminActor.Actor)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotNil(t, configs) {
		return
	}
	assert.Len(t, *configs, 0)
	favorites, err := svcClient.GetFavorites(ctx, adminActor, adminActor.Actor)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotNil(t, favorites) {
		return
	}
	assert.Len(t, *favorites, 0)
	recents, err := svcClient.GetRecents(ctx, adminActor, adminActor.Actor)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotNil(t, recents) {
		return
	}
	assert.Len(t, *recents, 0)

	// set a config settings
	err = svcClient.SetConfig(ctx, adminActor, adminActor.Actor, "foo1", "bar1")
	if !assert.NoError(t, err) {
		return
	}
	configs, err = svcClient.GetConfigs(ctx, adminActor, adminActor.Actor)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotNil(t, configs) {
		return
	}
	assert.Lenf(t, *configs, 1, "%+v", configs)
	if !assert.Contains(t, *configs, "foo1") {
		return
	}
	assert.Equal(t, "bar1", (*configs)["foo1"])

	// set a recent settings
	err = svcClient.SetRecent(ctx, adminActor, adminActor.Actor, "foo2", "bar2")
	if !assert.NoError(t, err) {
		return
	}
	recents, err = svcClient.GetRecents(ctx, adminActor, adminActor.Actor)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotNil(t, recents) {
		return
	}
	assert.Lenf(t, *recents, 1, "%+v", recents)
	if !assert.Contains(t, *recents, "foo2") {
		return
	}
	assert.Equal(t, "bar2", (*recents)["foo2"])

	// add a favorite
	err = svcClient.AddFavorite(ctx, adminActor, adminActor.Actor, "foo3", "bar3")
	if !assert.NoError(t, err) {
		return
	}
	favorites, err = svcClient.GetFavorites(ctx, adminActor, adminActor.Actor)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotNil(t, favorites) {
		return
	}
	assert.Lenf(t, *favorites, 1, "%+v", favorites)
	if !assert.Contains(t, *favorites, "foo3") {
		return
	}
	assert.Lenf(t, (*favorites)["foo3"], 1, "%+v", (*favorites)["foo3"])
	assert.Equal(t, []string{"bar3"}, (*favorites)["foo3"])

	// add another favorite under the same key
	err = svcClient.AddFavorite(ctx, adminActor, adminActor.Actor, "foo3", "bar3_1")
	if !assert.NoError(t, err) {
		return
	}
	favorites, err = svcClient.GetFavorites(ctx, adminActor, adminActor.Actor)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotNil(t, favorites) {
		return
	}
	assert.Lenf(t, *favorites, 1, "%+v", favorites)
	if !assert.Contains(t, *favorites, "foo3") {
		return
	}
	assert.Lenf(t, (*favorites)["foo3"], 2, "%+v", (*favorites)["foo3"])
	assert.Contains(t, (*favorites)["foo3"], "bar3")
	assert.Contains(t, (*favorites)["foo3"], "bar3_1")

	settings, err = svcClient.GetSettings(ctx, adminActor, adminActor.Actor)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotNil(t, settings) {
		return
	}
	assert.Lenf(t, settings.Configs, 1, "%+v", settings.Configs)
	assert.Len(t, settings.Favorites, 1)
	assert.Len(t, settings.Recents, 1)

	// fetch user object with settings included
	userWithSettings, err := svcClient.Get(ctx, adminActor, adminActor.Actor, true)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotNil(t, userWithSettings) {
		return
	}
	assert.Equal(t, adminActor.Actor, userWithSettings.GetPrimaryID())
	assert.Equal(t, "Foo", userWithSettings.FirstName)
	assert.Equal(t, "Bar", userWithSettings.LastName)
	assert.Equal(t, "FooBar@example.com", userWithSettings.PrimaryEmail)
	assert.Equal(t, *settings, userWithSettings.UserSettings)

	// unauthorized user, non-admin user get other user's settings
	settings, err = svcClient.GetSettings(ctx, service.Actor{Actor: "nonadmin_user123"}, adminActor.Actor)
	if !assert.Errorf(t, err, "should error for unauthorized access") {
		return
	}
	assert.Equal(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError())
	assert.Nil(t, settings)
	configs, err = svcClient.GetConfigs(ctx, service.Actor{Actor: "nonadmin_user123"}, adminActor.Actor)
	if !assert.Errorf(t, err, "should error for unauthorized access") {
		return
	}
	assert.Equal(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError())
	assert.Nil(t, configs)
	favorites, err = svcClient.GetFavorites(ctx, service.Actor{Actor: "nonadmin_user123"}, adminActor.Actor)
	if !assert.Errorf(t, err, "should error for unauthorized access") {
		return
	}
	assert.Equal(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError())
	assert.Nil(t, favorites)
	recents, err = svcClient.GetRecents(ctx, service.Actor{Actor: "nonadmin_user123"}, adminActor.Actor)
	if !assert.Errorf(t, err, "should error for unauthorized access") {
		return
	}
	assert.Equal(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError())
	assert.Nil(t, recents)
	err = svcClient.SetConfig(ctx, service.Actor{Actor: "nonadmin_user123"}, adminActor.Actor, "foo1", "bar1")
	if !assert.Errorf(t, err, "should error for unauthorized access") {
		return
	}
	assert.Equal(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError())

	// admin user get & set other user's settings
	settings, err = svcClient.GetSettings(ctx, adminActor, "nonadmin_user123")
	if !assert.NoError(t, err) {
		return
	}
	assert.NotNil(t, settings)
	assert.Empty(t, settings.Configs)
	assert.Empty(t, settings.Favorites)
	assert.Empty(t, settings.Recents)
	err = svcClient.SetConfig(ctx, adminActor, "nonadmin_user123", "foo1", "bar1")
	if !assert.NoError(t, err) {
		return
	}
	settings, err = svcClient.GetSettings(ctx, adminActor, "nonadmin_user123")
	if !assert.NoError(t, err) {
		return
	}
	assert.NotNil(t, settings)
	if !assert.Len(t, settings.Configs, 1) {
		return
	}
	if !assert.Contains(t, settings.Configs, "foo1") {
		return
	}
	assert.Equal(t, "bar1", settings.Configs["foo1"])
	assert.Empty(t, settings.Favorites)
	assert.Empty(t, settings.Recents)
}

func TestUserConcurrentAdd(t *testing.T) {
	runIntegrationTest(t, testConcurrentGetOrCreate)
}

func testConcurrentGetOrCreate(t *testing.T, s types.Specification) {
	log.SetLevel(log.InfoLevel)
	ctx, cancelSvcClient := context.WithTimeout(context.Background(), time.Second*20)
	defer cancelSvcClient()
	natsConn, stanConn, err := createConnectionFromSpec(s)
	if err != nil {
		return
	}
	defer natsConn.Close()
	defer stanConn.Close()
	svcClient, err := service.NewNatsUserClientFromConn(natsConn, stanConn)
	if !assert.NoError(t, err) {
		return
	}

	const N = 100
	var wg sync.WaitGroup
	var addUserErrorCounter int64
	var addUserCounter int64
	var gotUserCounter int64
	wg.Add(N)
	for i := 0; i < N; i++ {
		go func() {
			defer wg.Done()
			// get or add, this pattern is used by api-service to create new user on the fly in its auth middleware
			fetchedUser, err := svcClient.Get(ctx, service.Actor{Actor: "RESERVED_CACAO_SYSTEM_ACTOR"}, "testuser123", false)
			if err == nil {
				assert.Equal(t, "testuser123", fetchedUser.Username)
				atomic.AddInt64(&gotUserCounter, 1)
				return
			}
			err = svcClient.Add(ctx, service.Actor{Actor: "RESERVED_CACAO_SYSTEM_ACTOR"}, service.UserModel{Username: "testuser123", FirstName: "Foo", LastName: "Bar"})
			if err == nil {
				atomic.AddInt64(&addUserCounter, 1)
				return
			}
			atomic.AddInt64(&addUserErrorCounter, 1)
			assert.Equal(t, service.CacaoAlreadyExistErrorMessage, service.ToCacaoError(err).StandardError())
		}()
	}
	wg.Wait()
	log.Info(addUserErrorCounter)
	assert.LessOrEqual(t, addUserErrorCounter, int64(N-1))
	assert.Equal(t, int64(1), addUserCounter) // only 1 call of Add() should succeeds
	assert.Equal(t, int64(N), gotUserCounter+addUserCounter+addUserErrorCounter)
}

func TestAnonymousUser(t *testing.T) {
	runIntegrationTest(t, testAnonymousUser)
}

func testAnonymousUser(t *testing.T, s types.Specification) {
	ctx, cancelSvcClient := context.WithTimeout(context.Background(), time.Second*5)
	defer cancelSvcClient()
	natsConn, stanConn, err := createConnectionFromSpec(s) // create separate connections for the service client
	if err != nil {
		return
	}
	defer natsConn.Close()
	defer stanConn.Close()
	svcClient, err := service.NewNatsUserClientFromConn(natsConn, stanConn)
	if !assert.NoError(t, err) {
		return
	}

	newUser := service.UserModel{Username: "other-user"}
	err = svcClient.Add(ctx, service.Actor{Actor: "RESERVED_CACAO_SYSTEM_ACTOR"}, newUser)
	if !assert.NoError(t, err) {
		return
	}
	err = svcClient.SetConfig(ctx, service.Actor{Actor: newUser.Username}, newUser.Username, "foo", "bar")
	if !assert.NoError(t, err) {
		return
	}

	t.Run("create user with anonymous username", func(t *testing.T) {
		err = svcClient.Add(ctx, service.Actor{Actor: service.ReservedCacaoSystemActor}, service.UserModel{
			Username:     service.ReservedAnonymousActor,
			FirstName:    "Foo",
			LastName:     "Bar",
			PrimaryEmail: "foobar@example.com",
		})
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})

	t.Run("anonymous user get self", func(t *testing.T) {
		user, err := svcClient.Get(ctx, service.Actor{Actor: service.ReservedAnonymousActor}, service.ReservedAnonymousActor, false)
		if !assert.Error(t, err) {
			return
		}
		assert.Nil(t, user)
		assert.Equalf(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError(), err.Error())
		assert.Equal(t, types.GeneralActorNotAuthorizedError, service.ToCacaoError(err).ContextualError())
	})
	t.Run("anonymous user get other", func(t *testing.T) {
		user, err := svcClient.Get(ctx, service.Actor{Actor: service.ReservedAnonymousActor}, newUser.Username, false)
		if !assert.Error(t, err) {
			return
		}
		assert.Nil(t, user)
		assert.Equalf(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError(), err.Error())
		assert.Equal(t, types.GeneralActorNotAuthorizedError, service.ToCacaoError(err).ContextualError())
	})
	t.Run("anonymous user get other non-existent", func(t *testing.T) {
		user, err := svcClient.Get(ctx, service.Actor{Actor: service.ReservedAnonymousActor}, "do-not-exists-123", false)
		if !assert.Error(t, err) {
			return
		}
		assert.Nil(t, user)
		assert.Equalf(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError(), err.Error())
		assert.Equal(t, types.GeneralActorNotAuthorizedError, service.ToCacaoError(err).ContextualError())
	})

	t.Run("anonymous user list user", func(t *testing.T) {
		userList, err := svcClient.Search(ctx, service.Actor{Actor: service.ReservedAnonymousActor}, service.UserListFilter{MaxItems: 10})
		assert.Nil(t, userList)
		if !assert.Error(t, err) {
			return
		}
		assert.Equalf(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError(), err.Error())
		assert.Equal(t, types.GeneralActorNotAuthorizedError, service.ToCacaoError(err).ContextualError())
	})

	t.Run("anonymous user creates other user", func(t *testing.T) {
		err = svcClient.Add(ctx, service.Actor{Actor: service.ReservedAnonymousActor}, service.UserModel{
			Username:     "other-user123",
			FirstName:    "Foo",
			LastName:     "Bar",
			PrimaryEmail: "foobar@example.com",
		})
		if !assert.Error(t, err) {
			return
		}
		assert.Equalf(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError(), err.Error())
		assert.Equal(t, types.GeneralActorNotAuthorizedError, service.ToCacaoError(err).ContextualError())
	})
	t.Run("anonymous user creates self", func(t *testing.T) {
		err = svcClient.Add(ctx, service.Actor{Actor: service.ReservedAnonymousActor}, service.UserModel{
			Username:     service.ReservedAnonymousActor,
			FirstName:    "Foo",
			LastName:     "Bar",
			PrimaryEmail: "foobar@example.com",
		})
		if !assert.Error(t, err) {
			return
		}
		assert.Equalf(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError(), err.Error())
		assert.Equal(t, types.UserUsernameReservedCannotAddError, service.ToCacaoError(err).ContextualError())
	})

	t.Run("anonymous user updates self", func(t *testing.T) {
		err = svcClient.Update(ctx, service.Actor{Actor: service.ReservedAnonymousActor}, service.UserModel{
			Username:  service.ReservedAnonymousActor,
			FirstName: "Foo",
		})
		if !assert.Error(t, err) {
			return
		}
		assert.Equalf(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError(), err.Error())
		assert.Equal(t, types.GeneralActorNotAuthorizedError, service.ToCacaoError(err).ContextualError())
	})
	t.Run("anonymous user update other", func(t *testing.T) {
		err = svcClient.Update(ctx, service.Actor{Actor: service.ReservedAnonymousActor}, service.UserModel{
			Username:  newUser.Username,
			FirstName: "Foo",
		})
		if !assert.Error(t, err) {
			return
		}
		assert.Equalf(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError(), err.Error())
		assert.Equal(t, types.GeneralActorNotAuthorizedError, service.ToCacaoError(err).ContextualError())
	})

	t.Run("anonymous user deletes self", func(t *testing.T) {
		err = svcClient.Delete(ctx, service.Actor{Actor: service.ReservedAnonymousActor}, service.UserModel{Username: service.ReservedAnonymousActor})
		if !assert.Error(t, err) {
			return
		}
		assert.Equalf(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError(), err.Error())
		assert.Equal(t, types.GeneralActorNotAuthorizedError, service.ToCacaoError(err).ContextualError())
	})
	t.Run("anonymous user deletes others", func(t *testing.T) {
		err = svcClient.Delete(ctx, service.Actor{Actor: service.ReservedAnonymousActor}, service.UserModel{Username: service.ReservedAnonymousActor})
		if !assert.Error(t, err) {
			return
		}
		assert.Equalf(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError(), err.Error())
		assert.Equal(t, types.GeneralActorNotAuthorizedError, service.ToCacaoError(err).ContextualError())
	})

	t.Run("anonymous user get self settings", func(t *testing.T) {
		settings, err := svcClient.GetSettings(ctx, service.Actor{Actor: service.ReservedAnonymousActor}, service.ReservedAnonymousActor)
		if !assert.Error(t, err) {
			return
		}
		assert.Equalf(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError(), err.Error())
		assert.Equal(t, types.GeneralActorNotAuthorizedError, service.ToCacaoError(err).ContextualError())
		if !assert.Nil(t, settings) {
			return
		}
	})
	t.Run("anonymous user get others' settings", func(t *testing.T) {
		settings, err := svcClient.GetSettings(ctx, service.Actor{Actor: service.ReservedAnonymousActor}, newUser.Username)
		if !assert.Error(t, err) {
			return
		}
		assert.Equalf(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError(), err.Error())
		assert.Equal(t, types.GeneralActorNotAuthorizedError, service.ToCacaoError(err).ContextualError())
		if !assert.Nil(t, settings) {
			return
		}
	})

	t.Run("anonymous user sets self configs", func(t *testing.T) {
		err := svcClient.SetConfig(ctx, service.Actor{Actor: service.ReservedAnonymousActor}, service.ReservedAnonymousActor, "foo", "bar")
		if !assert.Error(t, err) {
			return
		}
		assert.Equalf(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError(), err.Error())
		assert.Equal(t, types.GeneralActorNotAuthorizedError, service.ToCacaoError(err).ContextualError())
	})
	t.Run("anonymous user sets others' configs", func(t *testing.T) {
		err := svcClient.SetConfig(ctx, service.Actor{Actor: service.ReservedAnonymousActor}, service.ReservedAnonymousActor, "foo", "bar")
		if !assert.Error(t, err) {
			return
		}
		assert.Equalf(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError(), err.Error())
		assert.Equal(t, types.GeneralActorNotAuthorizedError, service.ToCacaoError(err).ContextualError())
	})
}

func TestSystemUser(t *testing.T) {
	runIntegrationTest(t, testSystemUser)
}

func testSystemUser(t *testing.T, s types.Specification) {
	ctx, cancelSvcClient := context.WithTimeout(context.Background(), time.Second*5)
	defer cancelSvcClient()
	natsConn, stanConn, err := createConnectionFromSpec(s) // create separate connections for the service client
	if err != nil {
		return
	}
	defer natsConn.Close()
	defer stanConn.Close()
	svcClient, err := service.NewNatsUserClientFromConn(natsConn, stanConn)
	if !assert.NoError(t, err) {
		return
	}

	newUser := service.UserModel{Username: "testuser123"}
	t.Run("system user create other user", func(t *testing.T) {
		err = svcClient.Add(ctx, service.Actor{Actor: "RESERVED_CACAO_SYSTEM_ACTOR"}, newUser)
		assert.NoError(t, err)
	})
	t.Run("system user create user with system username", func(t *testing.T) {
		err = svcClient.Add(ctx, service.Actor{Actor: "RESERVED_CACAO_SYSTEM_ACTOR"}, service.UserModel{Username: service.ReservedCacaoSystemActor})
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("system user create user with anonymous username", func(t *testing.T) {
		err = svcClient.Add(ctx, service.Actor{Actor: "RESERVED_CACAO_SYSTEM_ACTOR"}, service.UserModel{Username: service.ReservedAnonymousActor})
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("system user get other user", func(t *testing.T) {
		_ = svcClient.Add(ctx, service.Actor{Actor: service.ReservedCacaoSystemActor}, newUser)
		user, err := svcClient.Get(ctx, service.Actor{Actor: service.ReservedCacaoSystemActor}, newUser.Username, false)
		assert.NoError(t, err)
		assert.NotNil(t, user)
	})
	t.Run("system user get self", func(t *testing.T) {
		user, err := svcClient.Get(ctx, service.Actor{Actor: service.ReservedCacaoSystemActor}, service.ReservedCacaoSystemActor, false)
		assert.Nil(t, user)
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoNotFoundErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("system user list user", func(t *testing.T) {
		userList, err := svcClient.Search(ctx, service.Actor{Actor: service.ReservedCacaoSystemActor}, service.UserListFilter{MaxItems: 10})
		assert.Nil(t, userList)
		if !assert.Error(t, err) {
			return
		}
		assert.Equalf(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError(), err.Error())
		assert.Equal(t, types.GeneralActorNotAuthorizedError, service.ToCacaoError(err).ContextualError())
	})
	t.Run("system user sets config for other user", func(t *testing.T) {
		err = svcClient.SetConfig(ctx, service.Actor{Actor: service.ReservedCacaoSystemActor}, newUser.Username, "foo", "bar")
		if !assert.Error(t, err) {
			return
		}
		assert.Equalf(t, service.CacaoUnauthorizedErrorMessage, service.ToCacaoError(err).StandardError(), err.Error())
		assert.Equal(t, types.GeneralActorNotAuthorizedError, service.ToCacaoError(err).ContextualError())
	})
}

func TestUserLimits(t *testing.T) {
	runIntegrationTest(t, testUserLimits)
}

func testUserLimits(t *testing.T, s types.Specification) {
	ctx, cancelSvcClient := context.WithTimeout(context.Background(), time.Second*30)
	defer cancelSvcClient()
	natsConn, stanConn, err := createConnectionFromSpec(s) // create separate connections for the service client
	if err != nil {
		return
	}
	defer natsConn.Close()
	defer stanConn.Close()
	svcClient, err := service.NewNatsUserClientFromConn(natsConn, stanConn)
	if !assert.NoError(t, err) {
		return
	}

	adminUser := service.UserModel{
		Username:     "adminuser123",
		PrimaryEmail: "",
		IsAdmin:      true,
	}
	err = svcClient.Add(ctx, service.Actor{Actor: service.ReservedCacaoSystemActor}, adminUser)
	if !assert.NoError(t, err) {
		return
	}
	adminActor := service.Actor{Actor: adminUser.Username}

	t.Run("create user with long username", func(t *testing.T) {
		newUser := service.UserModel{
			Username: strings.Repeat("a", types.MaxUsernameLength+1),
		}
		err = svcClient.Add(ctx, service.Actor{Actor: service.ReservedCacaoSystemActor}, newUser)
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("create user with long firstname", func(t *testing.T) {
		newUser := service.UserModel{
			Username:  common.NewID("user").String(), // use unique name to avoid collision with other tests
			FirstName: strings.Repeat("a", types.MaxFirstLastNameLength+1),
		}
		err = svcClient.Add(ctx, service.Actor{Actor: service.ReservedCacaoSystemActor}, newUser)
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("create user with long lastname", func(t *testing.T) {
		newUser := service.UserModel{
			Username: common.NewID("user").String(), // use unique name to avoid collision with other tests
			LastName: strings.Repeat("a", types.MaxFirstLastNameLength+1),
		}
		err = svcClient.Add(ctx, service.Actor{Actor: service.ReservedCacaoSystemActor}, newUser)
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("create user with long email", func(t *testing.T) {
		newUser := service.UserModel{
			Username:     common.NewID("user").String(), // use unique name to avoid collision with other tests
			PrimaryEmail: strings.Repeat("a", types.MaxEmailLength+1),
		}
		err = svcClient.Add(ctx, service.Actor{Actor: service.ReservedCacaoSystemActor}, newUser)
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})

	t.Run("update user with long firstname", func(t *testing.T) {
		newUser := service.UserModel{
			Username:     common.NewID("user").String(), // use unique name to avoid collision with other tests
			FirstName:    "Foo",
			LastName:     "Bar",
			PrimaryEmail: "FooBar@example.com",
		}
		err = svcClient.Add(ctx, service.Actor{Actor: service.ReservedCacaoSystemActor}, newUser)
		if !assert.NoError(t, err) {
			return
		}
		newUser.FirstName = strings.Repeat("a", types.MaxFirstLastNameLength+1)
		err = svcClient.Update(ctx, adminActor, newUser)
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("update user with long lastname", func(t *testing.T) {
		newUser := service.UserModel{
			Username:     common.NewID("user").String(), // use unique name to avoid collision with other tests
			FirstName:    "Foo",
			LastName:     "Bar",
			PrimaryEmail: "FooBar@example.com",
		}
		err = svcClient.Add(ctx, service.Actor{Actor: service.ReservedCacaoSystemActor}, newUser)
		if !assert.NoError(t, err) {
			return
		}
		newUser.LastName = strings.Repeat("a", types.MaxFirstLastNameLength+1)
		err = svcClient.Update(ctx, adminActor, newUser)
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("update user with long email", func(t *testing.T) {
		newUser := service.UserModel{
			Username:     common.NewID("user").String(), // use unique name to avoid collision with other tests
			FirstName:    "Foo",
			LastName:     "Bar",
			PrimaryEmail: "FooBar@example.com",
		}
		err = svcClient.Add(ctx, service.Actor{Actor: service.ReservedCacaoSystemActor}, newUser)
		if !assert.NoError(t, err) {
			return
		}
		newUser.PrimaryEmail = strings.Repeat("a", types.MaxEmailLength+1)
		err = svcClient.Update(ctx, adminActor, newUser)
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})

	t.Run("user sets too many configs key-value pair", func(t *testing.T) {
		newUser := service.UserModel{
			Username: common.NewID("user").String(), // use unique name to avoid collision with other tests
		}
		err = svcClient.Add(ctx, adminActor, newUser)
		if !assert.NoError(t, err) {
			return
		}
		for i := 0; i < types.MaxSettingsConfigPairs; i++ {
			err := svcClient.SetConfig(ctx, adminActor, newUser.Username, fmt.Sprintf("foo-%d", i), "bar")
			if !assert.NoError(t, err) {
				return
			}
		}
		err := svcClient.SetConfig(ctx, adminActor, newUser.Username, "last-key", "bar")
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("user sets too many favorite key-values pair", func(t *testing.T) {
		newUser := service.UserModel{
			Username: common.NewID("user").String(), // use unique name to avoid collision with other tests
		}
		err = svcClient.Add(ctx, service.Actor{Actor: service.ReservedCacaoSystemActor}, newUser)
		if !assert.NoError(t, err) {
			return
		}
		for i := 0; i < types.MaxSettingsFavoritePairs; i++ {
			err := svcClient.AddFavorite(ctx, adminActor, newUser.Username, fmt.Sprintf("foo-%d", i), "bar")
			if !assert.NoErrorf(t, err, "index: %d", i) {
				return
			}
		}
		err := svcClient.AddFavorite(ctx, adminActor, newUser.Username, "last-key", "bar")
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
		favorites, err := svcClient.GetFavorites(ctx, adminActor, newUser.Username)
		if !assert.NoError(t, err) {
			return
		}
		assert.Len(t, *favorites, types.MaxSettingsFavoritePairs)
	})
	t.Run("user adds too many favorite items under the same key", func(t *testing.T) {
		newUser := service.UserModel{
			Username: common.NewID("user").String(), // use unique name to avoid collision with other tests
		}
		err = svcClient.Add(ctx, adminActor, newUser)
		if !assert.NoError(t, err) {
			return
		}
		for i := 0; i < types.MaxFavoriteItemsPerKey; i++ {
			err := svcClient.AddFavorite(ctx, adminActor, newUser.Username, "same-key", fmt.Sprintf("item-%d", i))
			if !assert.NoError(t, err) {
				return
			}
		}
		err := svcClient.AddFavorite(ctx, adminActor, newUser.Username, "same-key", "last-item")
		assert.Error(t, err)
		if err != nil {
			assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
		}
		favorites, err := svcClient.GetFavorites(ctx, adminActor, newUser.Username)
		if !assert.NoError(t, err) {
			return
		}
		if !assert.Len(t, *favorites, 1) {
			return
		}
		assert.Len(t, (*favorites)["same-key"], types.MaxFavoriteItemsPerKey)
	})
	t.Run("user sets too many recents key-value pair", func(t *testing.T) {
		newUser := service.UserModel{
			Username: common.NewID("user").String(), // use unique name to avoid collision with other tests
		}
		err = svcClient.Add(ctx, adminActor, newUser)
		if !assert.NoError(t, err) {
			return
		}
		for i := 0; i < types.MaxSettingsRecentPairs; i++ {
			err := svcClient.SetRecent(ctx, adminActor, newUser.Username, fmt.Sprintf("foo-%d", i), "bar")
			if !assert.NoError(t, err) {
				return
			}
		}
		err := svcClient.SetRecent(ctx, adminActor, newUser.Username, "last-key", "bar")
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})

	t.Run("user sets config with long key", func(t *testing.T) {
		newUser := service.UserModel{
			Username: common.NewID("user").String(), // use unique name to avoid collision with other tests
		}
		err = svcClient.Add(ctx, adminActor, newUser)
		if !assert.NoError(t, err) {
			return
		}
		err = svcClient.SetConfig(ctx, adminActor, newUser.Username, strings.Repeat("a", types.MaxSettingsKeyLength+1), "bar")
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("user sets config with long value", func(t *testing.T) {
		newUser := service.UserModel{
			Username: common.NewID("user").String(), // use unique name to avoid collision with other tests
		}
		err = svcClient.Add(ctx, adminActor, newUser)
		if !assert.NoError(t, err) {
			return
		}
		err = svcClient.SetConfig(ctx, adminActor, newUser.Username, "foo", strings.Repeat("a", types.MaxSettingsValueLength+1))
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("user sets favorites with long key", func(t *testing.T) {
		newUser := service.UserModel{
			Username: common.NewID("user").String(), // use unique name to avoid collision with other tests
		}
		err = svcClient.Add(ctx, adminActor, newUser)
		if !assert.NoError(t, err) {
			return
		}
		err = svcClient.AddFavorite(ctx, adminActor, newUser.Username, strings.Repeat("a", types.MaxSettingsKeyLength+1), "bar")
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("user sets favorites with long value", func(t *testing.T) {
		newUser := service.UserModel{
			Username: common.NewID("user").String(), // use unique name to avoid collision with other tests
		}
		err = svcClient.Add(ctx, adminActor, newUser)
		if !assert.NoError(t, err) {
			return
		}
		err = svcClient.AddFavorite(ctx, adminActor, newUser.Username, "foo", strings.Repeat("a", types.MaxSettingsValueLength+1))
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("user sets recents with long key", func(t *testing.T) {
		newUser := service.UserModel{
			Username: common.NewID("user").String(), // use unique name to avoid collision with other tests
		}
		err = svcClient.Add(ctx, adminActor, newUser)
		if !assert.NoError(t, err) {
			return
		}
		err = svcClient.SetRecent(ctx, adminActor, newUser.Username, strings.Repeat("a", types.MaxSettingsKeyLength+1), "bar")
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})
	t.Run("user sets recents with long value", func(t *testing.T) {
		newUser := service.UserModel{
			Username: common.NewID("user").String(), // use unique name to avoid collision with other tests
		}
		err = svcClient.Add(ctx, adminActor, newUser)
		if !assert.NoError(t, err) {
			return
		}
		err = svcClient.SetRecent(ctx, adminActor, newUser.Username, "foo", strings.Repeat("a", types.MaxSettingsValueLength+1))
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoInvalidParameterErrorMessage, service.ToCacaoError(err).StandardError())
	})
}

func loadConfigFromEnv(t *testing.T) types.Specification {
	var s types.Specification
	err := envconfig.Process(types.ConfigVarPrefix, &s)
	if err != nil {
		t.Fatal(err)
	}
	return s
}

func createConnectionFromSpec(spec types.Specification) (*messaging2.NatsConnection, *messaging2.StanConnection, error) {
	natsConfig, stanConfig := types.StanConfigFromSpec(spec)
	var conf = messaging2.NatsStanMsgConfig{
		NatsConfig: natsConfig,
		StanConfig: stanConfig,
	}
	conf.NatsConfig.ClientID = common.NewID("e2etests").String()
	natsConn, err := conf.ConnectNats()
	if err != nil {
		return nil, nil, err
	}
	stanConn, err := conf.ConnectStan()
	if err != nil {
		natsConn.Close()
		return nil, nil, err
	}
	return &natsConn, &stanConn, nil
}

func skipTestIfNoMongoDB(t *testing.T) bool {
	val, ok := os.LookupEnv("CI_INTEGRATION_TEST_MONGO")
	if !ok {
		t.Skip("CI_INTEGRATION_TEST_MONGO not set")
		return true
	}
	if val != "true" {
		t.Skip("CI_INTEGRATION_TEST_MONGO not set")
		return true
	}
	return false
}

func skipTestIfNoNATS(t *testing.T) bool {
	val, ok := os.LookupEnv("CI_INTEGRATION_TEST_NATS")
	if !ok {
		t.Skip("CI_INTEGRATION_TEST_NATS not set")
		return true
	}
	if val != "true" {
		t.Skip("CI_INTEGRATION_TEST_NATS not set")
		return true
	}
	return false
}
