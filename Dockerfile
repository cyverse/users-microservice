# syntax=docker/dockerfile:1
ARG CACAO_BUILD_IMAGE="golang:1.23-bookworm"
ARG CACAO_FINAL_IMAGE="gcr.io/distroless/base-debian12:nonroot"
ARG SKAFFOLD_GO_GCFLAGS

FROM ${CACAO_BUILD_IMAGE} as base
COPY ./ /users-microservice/
WORKDIR /users-microservice/
ENV GOPROXY=direct
#ENV GOSUMDB=off
RUN --mount=type=cache,target=/root/.cache/go-build  eval go build -gcflags="${SKAFFOLD_GO_GCFLAGS}"

FROM ${CACAO_FINAL_IMAGE}
ARG GOTRACEBACK
ENV GOTRACEBACK="$GOTRACEBACK"
ARG VSCODE_DEBUG_SLEEP_SECONDS
ENV VSCODE_DEBUG_SLEEP_SECONDS="$VSCODE_DEBUG_SLEEP_SECONDS"
COPY --from=base /users-microservice/ /users-microservice/
WORKDIR /
CMD ["/users-microservice/users-microservice"]
