package main

import (
	"context"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"io"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"
	"users-microservice/adapters"
	"users-microservice/domain"
	"users-microservice/types"

	log "github.com/sirupsen/logrus"

	"github.com/kelseyhightower/envconfig"
)

func main() {

	log.SetReportCaller(false)

	var s types.Specification
	err := envconfig.Process(types.ConfigVarPrefix, &s)
	if err != nil {
		log.WithError(err).Fatal("fail to read Specification from env var")
	}

	log.Debug("main(): setting a cancelable context")
	ctx, cancel := context.WithCancel(context.Background())
	cancelWhenSignaled(cancel)
	defer cancel()
	s.AppContext = ctx // the ctx is passed along in the specification to be used in adapters

	// set the debug level
	loglvl, _ := log.ParseLevel(s.LogLevel)
	log.SetLevel(loglvl)

	// when using cloud code in vscode, sleep is needed to allow time for the debugger to attach fully
	// if the vscode debug session does not attach in time, increase the time in skaffold.yaml
	vscodeDebugSleepSecs := os.Getenv("VSCODE_DEBUG_SLEEP_SECONDS")
	if vscodeDebugSleepSecs != "" && vscodeDebugSleepSecs != "0" {
		var secs int
		var err error
		secs, err = strconv.Atoi(vscodeDebugSleepSecs)
		if err != nil {
			log.Debug("Warning: VSCODE_DEBUG_SLEEP_SECONDS was not a valid integer to sleep, will not sleep")
		} else {
			log.Debug("VSCODE_DEBUG_SLEEP_SECONDS found, sleeping for " + vscodeDebugSleepSecs + "s")
			time.Sleep(time.Duration(secs) * time.Second)
		}
	}

	log.WithField("specification", s).Debug()
	log.WithField("loglevel", loglvl).Debug()
	log.WithField("database URI", s.DbURI).Debug()
	log.WithField("database name", s.DbName).Debug()
	log.WithField("Nats URL", s.NatsURL).Debug()

	msgConf := types.MessagingConfigFromSpec(s)
	stanConn, err := messaging2.NewStanConnectionFromConfigWithoutEventSource(msgConf)
	if err != nil {
		log.WithError(err).Panic()
		return
	}
	defer logCloserError(&stanConn)

	// create an initial Domain object
	var ea = adapters.NewEventAdapter(&stanConn)
	defer logCloserError(ea)
	var qa adapters.QueryAdapter
	defer logCloserError(&qa)
	var storage adapters.MongoAdapter
	defer logCloserError(&storage)
	var dmain = domain.NewDomain(
		&qa, ea, &storage,
	)

	// initialize the domain and all the adapters in the Init method
	// start the domain, passing the context
	err = dmain.Init(s)
	if err != nil {
		log.WithError(err).Panic("fail to init domain")
	}
	dmain.Start(ctx)
}

func cancelWhenSignaled(cancelFunc context.CancelFunc) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM) // os.Interrupt for Ctrl-C, SIGTERM for k8s
	go func() {
		<-c
		cancelFunc()
	}()
}

func logCloserError(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.WithError(err).Error()
		return
	}
}
