package ports

import (
	"context"
	"sync"
	"users-microservice/types"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// Port is the base interface for Ports, in which all should support an Init() function (but it may be noop)
type Port interface {
	Init(s types.Specification) error
}

// AsyncPort is a special type of port that will has an asynchronous behavior to it, such as being event-driven
// outside of the primary domain thread (e.g. incoming message queues). This ports should communicate with the
// domain object using a threaded approach (i.e. go routine, go channels and possibly waitgroups)
type AsyncPort interface {
	Port
	Start(context.Context, *sync.WaitGroup) error
	Close() error
}

// IncomingQueryPort is an example interface for a query port.
// Internal to the adapter there may be additional methods, but at the very least, it is an async port
// Also, incoming and outgoing ports are declared separately for illustrative purposes
// method set signatures will allow one adapter fullfill the purposes of both income and outoing query ports
type IncomingQueryPort interface {
	AsyncPort
	SetQueryHandlers(handlers IncomingQueryHandlers)
}

// IncomingQueryHandlers ...
type IncomingQueryHandlers interface {
	UsersGet(ctx context.Context, query service.UserModel) service.UserModel
	UsersGetWithSettings(ctx context.Context, query service.UserModel) service.UserModel
	UserSettingsGet(ctx context.Context, query service.UserSettingsRequest) (service.UserSettingsReply, service.CacaoError)
	UserConfigsGet(ctx context.Context, query service.UserSettingsRequest) (service.UserSettingsReply, service.CacaoError)
	UserFavoritesGet(ctx context.Context, query service.UserSettingsRequest) (service.UserSettingsReply, service.CacaoError)
	UserRecentsGet(ctx context.Context, query service.UserSettingsRequest) (service.UserSettingsReply, service.CacaoError)
	UsersList(ctx context.Context, query service.UserListModel) service.UserListModel
}

// IncomingEventsPort is an example interface for an event port.
type IncomingEventsPort interface {
	AsyncPort
	SetHandlers(handlers IncomingEventHandlers)
}

// IncomingEventHandlers represents the handlers for all event operations supported by this service.
type IncomingEventHandlers interface {
	// users

	UserAddRequested(context.Context, service.UserModel, OutgoingEvents)
	UserUpdateRequested(context.Context, service.UserModel, OutgoingEvents)
	UserDeleteRequested(context.Context, service.UserModel, OutgoingEvents)

	// user settings

	UserConfigSetRequested(context.Context, service.UserSettingsRequest, OutgoingEvents)
	UserFavoriteAddedRequested(context.Context, service.UserSettingsRequest, OutgoingEvents)
	UserFavoriteDeletedRequested(context.Context, service.UserSettingsRequest, OutgoingEvents)
	UserRecentSetRequested(context.Context, service.UserSettingsRequest, OutgoingEvents)
}

// OutgoingEvents is an abstraction that allows implementation of
// IncomingEventHandlers to publish events, this represents all events that can
// be published by this service.
type OutgoingEvents interface {
	EventUserAdded(service.UserModel)
	EventUserAddError(service.UserModel)
	EventUserUpdated(service.UserModel)
	EventUserUpdateError(service.UserModel)
	EventUserDeleted(service.UserModel)
	EventUserDeleteError(service.UserModel)

	EventUserConfigSet(service.UserSettingsRequest)
	EventUserConfigSetError(service.UserSettingsRequest)
	EventUserFavoriteAdded(service.UserSettingsRequest)
	EventUserFavoriteAddError(service.UserSettingsRequest)
	EventUserFavoriteDeleted(service.UserSettingsRequest)
	EventUserFavoriteDeleteError(service.UserSettingsRequest)
	EventUserRecentSet(service.UserSettingsRequest)
	EventUserRecentSetError(service.UserSettingsRequest)
}

// PersistentStoragePort is a port to store messages
type PersistentStoragePort interface {
	Port
	UserAdd(context.Context, *types.DomainUserModel) error
	UserDelete(context.Context, string) error
	UserGet(ctx context.Context, username string) (types.DomainUserModel, error)
	UserExists(ctx context.Context, username string) (exists bool, err error)
	UserIsAdmin(ctx context.Context, username string) (isAdmin bool, exists bool, err error)
	UserUpdate(context.Context, *types.DomainUserModel) error
	UserList(context.Context, service.UserListFilter) ([]types.DomainUserModel, error)
	UserSettingsGet(context.Context, string, *common.UserSettings) error
	UserConfigSet(context.Context, string, string, string) error
	UserFavoriteAdd(context.Context, string, string, string) error
	UserFavoriteDelete(context.Context, string, string, string) error
	UserRecentSet(context.Context, string, string, string) error
}
