package domain

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"testing"
	"time"
	portsmocks "users-microservice/ports/mocks"
	"users-microservice/types"
)

func TestDomain_UsersGet(t *testing.T) {
	type fields struct {
		UserStore *portsmocks.PersistentStoragePort
	}
	type args struct {
		ctx   context.Context
		query service.UserModel
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   service.UserModel
	}{
		{
			name: "empty",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "").Return(false, false, nil)
					return storage
				}(),
			},
			args: args{
				ctx:   context.TODO(),
				query: service.UserModel{}, // empty request
			},
			want: service.UserModel{
				Session: service.Session{
					SessionActor:    "",
					SessionEmulator: "",
					ServiceError:    service.NewCacaoNotFoundError(types.GeneralActorNotFoundError).GetBase(),
				},
			},
		},
		{
			name: "auth check fail",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "testuser123").Return(false, false, fmt.Errorf("error123"))
					return storage
				}(),
			},
			args: args{
				ctx: context.TODO(),
				query: service.UserModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username: "testuser123",
				},
			},
			want: service.UserModel{
				Session: service.Session{
					SessionActor:    "testuser123",
					SessionEmulator: "",
					ServiceError:    service.NewCacaoGeneralError("error123").GetBase(),
				},
				Username: "testuser123",
			},
		},
		{
			name: "storage error while get self",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "testuser123").Return(false, true, nil)
					storage.On("UserGet", context.TODO(), "testuser123").Return(types.DomainUserModel{}, fmt.Errorf("error123"))
					return storage
				}(),
			},
			args: args{
				ctx: context.TODO(),
				query: service.UserModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username: "testuser123",
				},
			},
			want: service.UserModel{
				Session: service.Session{
					SessionActor:    "testuser123",
					SessionEmulator: "",
					ServiceError:    service.NewCacaoGeneralError("error123").GetBase(),
				},
				Username: "testuser123",
			},
		},
		{
			name: "empty actor",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "").Return(false, false, nil)
					return storage
				}(),
			},
			args: args{
				ctx: context.TODO(),
				query: service.UserModel{
					Session:  service.Session{},
					Username: "testuser123",
				},
			},
			want: service.UserModel{
				Session: service.Session{
					SessionActor:    "",
					SessionEmulator: "",
					ServiceError:    service.NewCacaoNotFoundError(types.GeneralActorNotFoundError).GetBase(),
				},
				Username: "testuser123",
			},
		},
		{
			name: "actor get self",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "testuser123").Return(false, true, nil)
					storage.On("UserGet", context.TODO(), "testuser123").Return(types.DomainUserModel{
						Username:     "testuser123",
						FirstName:    "firstName",
						LastName:     "lastName",
						PrimaryEmail: "foo@example.com",
					}, nil)
					return storage
				}(),
			},
			args: args{
				ctx: context.TODO(),
				query: service.UserModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username: "testuser123",
				},
			},
			want: service.UserModel{
				Session: service.Session{
					SessionActor:    "testuser123",
					SessionEmulator: "",
				},
				Username:          "testuser123",
				FirstName:         "firstName",
				LastName:          "lastName",
				PrimaryEmail:      "foo@example.com",
				IsAdmin:           false,
				DisabledAt:        time.Time{},
				WithSettings:      false,
				UserSettings:      common.UserSettings{},
				CreatedAt:         time.Time{},
				UpdatedAt:         time.Time{},
				UpdatedBy:         "",
				UpdatedEmulatorBy: "",
			},
		},
		{
			name: "non-admin get other",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "testuser123").Return(false, true, nil)
					return storage
				}(),
			},
			args: args{
				ctx: context.TODO(),
				query: service.UserModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username: "diffuser123",
				},
			},
			want: service.UserModel{
				Session: service.Session{
					SessionActor:    "testuser123",
					SessionEmulator: "",
					ServiceError:    service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError).GetBase(),
				},
				Username: "diffuser123",
			},
		},
		{
			name: "admin get other",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "admin123").Return(true, true, nil)
					storage.On("UserGet", context.TODO(), "testuser123").Return(types.DomainUserModel{
						Username:     "testuser123",
						FirstName:    "firstName",
						LastName:     "lastName",
						PrimaryEmail: "foo@example.com",
					}, nil)
					return storage
				}(),
			},
			args: args{
				ctx: context.TODO(),
				query: service.UserModel{
					Session: service.Session{
						SessionActor:    "admin123",
						SessionEmulator: "",
					},
					Username: "testuser123",
				},
			},
			want: service.UserModel{
				Session: service.Session{
					SessionActor:    "admin123",
					SessionEmulator: "",
				},
				Username:          "testuser123",
				FirstName:         "firstName",
				LastName:          "lastName",
				PrimaryEmail:      "foo@example.com",
				IsAdmin:           false,
				DisabledAt:        time.Time{},
				WithSettings:      false,
				UserSettings:      common.UserSettings{},
				CreatedAt:         time.Time{},
				UpdatedAt:         time.Time{},
				UpdatedBy:         "",
				UpdatedEmulatorBy: "",
			},
		},
		{
			name: "system user get other",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserGet", context.TODO(), "testuser123").Return(types.DomainUserModel{
						Username:     "testuser123",
						FirstName:    "firstName",
						LastName:     "lastName",
						PrimaryEmail: "foo@example.com",
					}, nil)
					return storage
				}(),
			},
			args: args{
				ctx: context.TODO(),
				query: service.UserModel{
					Session: service.Session{
						SessionActor:    service.ReservedCacaoSystemActor,
						SessionEmulator: "",
					},
					Username: "testuser123",
				},
			},
			want: service.UserModel{
				Session: service.Session{
					SessionActor:    service.ReservedCacaoSystemActor,
					SessionEmulator: "",
				},
				Username:          "testuser123",
				FirstName:         "firstName",
				LastName:          "lastName",
				PrimaryEmail:      "foo@example.com",
				IsAdmin:           false,
				DisabledAt:        time.Time{},
				WithSettings:      false,
				UserSettings:      common.UserSettings{},
				CreatedAt:         time.Time{},
				UpdatedAt:         time.Time{},
				UpdatedBy:         "",
				UpdatedEmulatorBy: "",
			},
		},
		{
			name: "admin get non-existent",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "admin123").Return(true, true, nil)
					storage.On("UserGet", context.TODO(), "not_exist123").Return(types.DomainUserModel{}, service.NewCacaoNotFoundError(types.UserUsernameNotFoundError))
					return storage
				}(),
			},
			args: args{
				ctx: context.TODO(),
				query: service.UserModel{
					Session: service.Session{
						SessionActor:    "admin123",
						SessionEmulator: "",
					},
					Username: "not_exist123",
				},
			},
			want: service.UserModel{
				Session: service.Session{
					SessionActor:    "admin123",
					SessionEmulator: "",
					ServiceError:    service.NewCacaoNotFoundError(types.UserUsernameNotFoundError).GetBase(),
				},
				Username: "not_exist123",
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &Domain{
				UserStore: tt.fields.UserStore,
			}
			assert.Equalf(t, tt.want, d.UsersGet(tt.args.ctx, tt.args.query), "UsersGet(%v, %v)", tt.args.ctx, tt.args.query)
			tt.fields.UserStore.AssertExpectations(t)
		})
	}
}

func TestDomain_UsersList(t *testing.T) {
	type fields struct {
		UserStore *portsmocks.PersistentStoragePort
	}
	type args struct {
		ctx   context.Context
		query service.UserListModel
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   service.UserListModel
	}{
		{
			name: "empty",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "").Return(false, false, nil)
					return storage
				}(),
			},
			args: args{
				ctx:   context.TODO(),
				query: service.UserListModel{},
			},
			want: service.UserListModel{
				Session: service.Session{
					SessionActor:    "",
					SessionEmulator: "",
					ServiceError:    service.NewCacaoNotFoundError(types.GeneralActorNotFoundError).GetBase(),
				},
			},
		},
		{
			name: "auth check fail",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "testuser123").Return(false, false, fmt.Errorf("error123"))
					return storage
				}(),
			},
			args: args{
				ctx: context.TODO(),
				query: service.UserListModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
				},
			},
			want: service.UserListModel{
				Session: service.Session{
					SessionActor:    "testuser123",
					SessionEmulator: "",
					ServiceError:    service.NewCacaoGeneralError("error123").GetBase(),
				},
			},
		},
		{
			name: "storage error while non-admin get self",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "testuser123").Return(false, true, nil)
					storage.On("UserGet", context.TODO(), "testuser123").Return(types.DomainUserModel{}, fmt.Errorf("error123"))
					return storage
				}(),
			},
			args: args{
				ctx: context.TODO(),
				query: service.UserListModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Filter: service.UserListFilter{
						Field:           "",
						Value:           "",
						SortBy:          0,
						MaxItems:        123,
						Start:           0,
						IncludeDisabled: false,
					},
				},
			},
			want: service.UserListModel{
				Session: service.Session{
					SessionActor:    "testuser123",
					SessionEmulator: "",
					ServiceError:    service.NewCacaoGeneralError("error123").GetBase(),
				},
			},
		},
		{
			name: "empty actor",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "").Return(false, false, nil)
					return storage
				}(),
			},
			args: args{
				ctx: context.TODO(),
				query: service.UserListModel{
					Session: service.Session{},
				},
			},
			want: service.UserListModel{
				Session: service.Session{
					SessionActor:    "",
					SessionEmulator: "",
					ServiceError:    service.NewCacaoNotFoundError(types.GeneralActorNotFoundError).GetBase(),
				},
			},
		},
		{
			name: "non-admin get self",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "testuser123").Return(false, true, nil)
					storage.On("UserGet", context.TODO(), "testuser123").Return(types.DomainUserModel{
						Username:     "testuser123",
						FirstName:    "firstName",
						LastName:     "lastName",
						PrimaryEmail: "foo@example.com",
					}, nil)
					return storage
				}(),
			},
			args: args{
				ctx: context.TODO(),
				query: service.UserListModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
				},
			},
			want: service.UserListModel{
				Session: service.Session{
					SessionActor:    "testuser123",
					SessionEmulator: "",
				},
				Users: []service.UserModel{
					{
						Username:          "testuser123",
						FirstName:         "firstName",
						LastName:          "lastName",
						PrimaryEmail:      "foo@example.com",
						IsAdmin:           false,
						DisabledAt:        time.Time{},
						WithSettings:      false,
						UserSettings:      common.UserSettings{},
						CreatedAt:         time.Time{},
						UpdatedAt:         time.Time{},
						UpdatedBy:         "",
						UpdatedEmulatorBy: "",
					},
				},
			},
		},
		{
			name: "admin list users",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "admin123").Return(true, true, nil)
					storage.On("UserList", context.TODO(), service.UserListFilter{
						Field:           "",
						Value:           "",
						SortBy:          0,
						MaxItems:        123,
						Start:           0,
						IncludeDisabled: false,
					}).Return([]types.DomainUserModel{
						{
							Username:     "testuser123",
							FirstName:    "firstName1",
							LastName:     "lastName1",
							PrimaryEmail: "foo1@example.com",
						},
						{
							Username:     "admin123",
							FirstName:    "firstName2",
							LastName:     "lastName2",
							PrimaryEmail: "foo2@example.com",
						},
						{
							Username:     "testuser456",
							FirstName:    "firstName3",
							LastName:     "lastName3",
							PrimaryEmail: "foo3@example.com",
						},
					}, nil)
					return storage
				}(),
			},
			args: args{
				ctx: context.TODO(),
				query: service.UserListModel{
					Session: service.Session{
						SessionActor:    "admin123",
						SessionEmulator: "",
					},
					Filter: service.UserListFilter{
						Field:           "",
						Value:           "",
						SortBy:          0,
						MaxItems:        123,
						Start:           0,
						IncludeDisabled: false,
					},
					Users:      nil,
					StartIndex: 0,
					TotalSize:  0,
					NextStart:  0,
				},
			},
			want: service.UserListModel{
				Session: service.Session{
					SessionActor:    "admin123",
					SessionEmulator: "",
				},
				Users: []service.UserModel{
					{
						Username:     "testuser123",
						FirstName:    "firstName1",
						LastName:     "lastName1",
						PrimaryEmail: "foo1@example.com",
					},
					{
						Username:     "admin123",
						FirstName:    "firstName2",
						LastName:     "lastName2",
						PrimaryEmail: "foo2@example.com",
					},
					{
						Username:     "testuser456",
						FirstName:    "firstName3",
						LastName:     "lastName3",
						PrimaryEmail: "foo3@example.com",
					},
				},
			},
		},
		{
			name: "admin list users but missing max items",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "admin123").Return(true, true, nil)
					storage.On("UserList", context.TODO(), service.UserListFilter{
						Field:           "",
						Value:           "",
						SortBy:          0,
						MaxItems:        types.MaxUserListSize,
						Start:           0,
						IncludeDisabled: false,
					}).Return([]types.DomainUserModel{
						{
							Username:     "testuser123",
							FirstName:    "firstName1",
							LastName:     "lastName1",
							PrimaryEmail: "foo1@example.com",
						},
						{
							Username:     "admin123",
							FirstName:    "firstName2",
							LastName:     "lastName2",
							PrimaryEmail: "foo2@example.com",
						},
						{
							Username:     "testuser456",
							FirstName:    "firstName3",
							LastName:     "lastName3",
							PrimaryEmail: "foo3@example.com",
						},
					}, nil)
					return storage
				}(),
			},
			args: args{
				ctx: context.TODO(),
				query: service.UserListModel{
					Session: service.Session{
						SessionActor:    "admin123",
						SessionEmulator: "",
					},
					Filter: service.UserListFilter{
						Field:           "",
						Value:           "",
						SortBy:          0,
						MaxItems:        0, // MaxItems is 0
						Start:           0,
						IncludeDisabled: false,
					},
					Users:      nil,
					StartIndex: 0,
					TotalSize:  0,
					NextStart:  0,
				},
			},
			want: service.UserListModel{
				Session: service.Session{
					SessionActor:    "admin123",
					SessionEmulator: "",
				},
				Users: []service.UserModel{
					{
						Username:     "testuser123",
						FirstName:    "firstName1",
						LastName:     "lastName1",
						PrimaryEmail: "foo1@example.com",
					},
					{
						Username:     "admin123",
						FirstName:    "firstName2",
						LastName:     "lastName2",
						PrimaryEmail: "foo2@example.com",
					},
					{
						Username:     "testuser456",
						FirstName:    "firstName3",
						LastName:     "lastName3",
						PrimaryEmail: "foo3@example.com",
					},
				},
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &Domain{
				UserStore: tt.fields.UserStore,
			}
			assert.Equalf(t, tt.want, d.UsersList(tt.args.ctx, tt.args.query), "UsersList(%v, %v)", tt.args.ctx, tt.args.query)
		})
	}
}
