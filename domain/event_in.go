// Package domain ...
package domain

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"slices"
	"unicode"
	"users-microservice/ports"
	"users-microservice/types"
)

var _ ports.IncomingEventHandlers = (*Domain)(nil)

// UserAddRequested handles user creation request.
// Only admin or system user can create user.
func (d *Domain) UserAddRequested(ctx context.Context, request service.UserModel, sink ports.OutgoingEvents) {
	logger := log.WithFields(log.Fields{
		"function": "Domain.UserAddRequested",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"username": request.Username,
	})
	logger.Trace("starting")

	var response service.UserModel
	response.Session = service.CopySessionActors(request.Session)
	response.Username = request.Username

	if isReservedUsername(request) {
		logger.WithField("error", types.UserUsernameReservedCannotAddError).Warn()
		d.setErrorInSession(&response.Session, service.NewCacaoInvalidParameterError(types.UserUsernameReservedCannotAddError))
		sink.EventUserAddError(response)
		return
	}
	allowed, err := d.addUserAuthZCheck(ctx, request.Session, request.Username)
	if err != nil {
		logger.WithError(err).Error("admin check failed")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserAddError(response)
		return
	}
	if !allowed {
		d.setErrorInSession(&response.Session, service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError))
		sink.EventUserAddError(response)
		return
	}

	if request.Username == "" {
		d.setErrorInSession(&response.Session, service.NewCacaoInvalidParameterError("username not set, cannot add user"))
		sink.EventUserAddError(response)
		return
	}

	domainModel := types.NewDomainUserModel(request)
	err = validateUserCreationRequest(domainModel)
	if err != nil {
		logger.WithError(err).Trace("cannot add user, invalid request")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserAddError(response)
		return
	}
	err = d.UserStore.UserAdd(ctx, domainModel)
	if err == nil {
		// successfully added the user
		sink.EventUserAdded(response)
		return
	}
	logger.WithError(err).Tracef("fail to add user '%s'", request.Username)
	d.setErrorInSession(&response.Session, err)
	sink.EventUserAddError(response)
}

func validateUserCreationRequest(model *types.DomainUserModel) error {
	if model.Username == "" {
		return service.NewCacaoInvalidParameterError("username is empty")
	}
	if len(model.Username) > types.MaxUsernameLength {
		return service.NewCacaoInvalidParameterError("username too long")
	}
	if len(model.FirstName) > types.MaxFirstLastNameLength {
		return service.NewCacaoInvalidParameterError("first name too long")
	}
	if len(model.LastName) > types.MaxFirstLastNameLength {
		return service.NewCacaoInvalidParameterError("last name too long")
	}
	if len(model.PrimaryEmail) > types.MaxEmailLength {
		return service.NewCacaoInvalidParameterError("email too long")
	}
	return nil
}

// Use this to check if a username is reserved, and thus should NOT be created.
// TODO: future enhancement will check a list of other reserved usernames
func isReservedUsername(user service.UserModel) bool {
	return slices.Contains(types.ReservedUsernameList, user.Username)
}

// UserUpdateRequested handles user update request.
// admin user can update any user, non-admin user can only update non-protected fields of themselves.
func (d *Domain) UserUpdateRequested(ctx context.Context, request service.UserModel, sink ports.OutgoingEvents) {
	logger := log.WithFields(log.Fields{
		"function": "Domain.UserUpdateRequested",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"username": request.Username,
	})
	logger.Trace("starting")

	var response service.UserModel
	response.Session = service.CopySessionActors(request.Session)
	response.Username = request.Username

	allowed, actorIsAdmin, err := d.updateUserAuthZCheck(ctx, request.Session, request.Username)
	if err != nil {
		logger.WithError(err).Error("admin check failed")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserUpdateError(response)
		return
	}
	if !allowed {
		logger.WithField("error", types.GeneralActorNotAuthorizedError).Info("actor cannot update other user")
		d.setErrorInSession(&response.Session, service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError))
		sink.EventUserUpdateError(response)
		return
	}

	err = validateUpdateRequest(request)
	if err != nil {
		logger.WithError(err).Info("cannot update user, invalid request")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserUpdateError(response)
		return
	}

	origUser, err := d.UserStore.UserGet(ctx, request.Username)
	if err != nil {
		logger.WithError(err).Error("fail to get user before update")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserUpdateError(response)
		return
	}

	// if regular user, we need to copy over the protected fields
	if !actorIsAdmin {
		request.IsAdmin = origUser.IsAdmin       // admin state can only be changed by admins
		request.DisabledAt = origUser.DisabledAt // admin users can only change disabledAts
	}

	// we always protect these fields
	request.CreatedAt = origUser.CreatedAt

	err = d.UserStore.UserUpdate(ctx, types.NewDomainUserModel(request))
	if err != nil { // for now, assuming cannot add
		logger.WithError(err).Error("fail to update user in storage")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserUpdateError(response)
	} else {
		response.UpdatedBy = request.SessionActor
		response.UpdatedEmulatorBy = request.SessionEmulator
		sink.EventUserUpdated(response)
	}
}

func validateUpdateRequest(request service.UserModel) error {
	if len(request.FirstName) > types.MaxFirstLastNameLength {
		return service.NewCacaoInvalidParameterError("firstname is too long")
	}
	if len(request.LastName) > types.MaxFirstLastNameLength {
		return service.NewCacaoInvalidParameterError("lastname is too long")
	}
	if len(request.PrimaryEmail) > types.MaxEmailLength {
		return service.NewCacaoInvalidParameterError("email is too long")
	}
	return nil
}

// UserDeleteRequested handles deletion request.
func (d *Domain) UserDeleteRequested(ctx context.Context, request service.UserModel, sink ports.OutgoingEvents) {
	logger := log.WithFields(log.Fields{
		"function": "Domain.UserDeleteRequested",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"username": request.Username,
	})
	logger.Trace("starting")

	var response service.UserModel
	response.Session = service.CopySessionActors(request.Session)
	response.Username = request.Username

	allowed, err := d.deleteUserAuthZCheck(ctx, request.Session, request.Username)
	if err != nil {
		logger.WithError(err).Error("fail to check if actor is admin for deletion")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserDeleteError(response)
		return
	}
	if !allowed {
		logger.Info("actor is not admin, unable to delete user")
		d.setErrorInSession(&response.Session, service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError))
		sink.EventUserDeleteError(response)
		return
	}

	err = d.UserStore.UserDelete(ctx, request.Username)
	if err != nil {
		errType := d.setErrorInSession(&response.Session, err)
		if errType == service.CacaoNotFoundErrorMessage {
			logger.WithError(err).Info("fail to delete user in storage")
		} else {
			logger.WithError(err).Error("fail to delete user in storage")
		}
		sink.EventUserDeleteError(response)
	} else {
		sink.EventUserDeleted(response)
	}
}

// UserConfigSetRequested handles config set request.
func (d *Domain) UserConfigSetRequested(ctx context.Context, request service.UserSettingsRequest, sink ports.OutgoingEvents) {
	logger := log.WithFields(log.Fields{
		"function": "Domain.UserConfigSetRequested",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"username": request.Username,
	})
	logger.Trace("starting")
	var response service.UserSettingsRequest
	response.Username = request.Username

	allowed, err := d.defaultAuthZCheck(ctx, request.Session, request.Username)
	if err != nil {
		logger.WithError(err).Error("authZ check failed when setting config setting")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserConfigSetError(response)
		return
	}
	if !allowed {
		logger.WithError(err).Info("actor is unauthorized to set config")
		d.setErrorInSession(&response.Session, service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError))
		sink.EventUserConfigSetError(response)
		return
	}
	err = d.validateSettingsRequest(request)
	if err != nil {
		logger.WithError(err).Info("invalid request to set configs")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserConfigSetError(response)
		return
	}
	err = d.UserStore.UserConfigSet(ctx, request.Username, request.Key, request.Value)
	if err != nil {
		logger.WithError(err).Error("fail to set config settings in storage")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserConfigSetError(response)
		return
	}
	response.Key = request.Key
	response.Value = request.Value
	sink.EventUserConfigSet(response)
}

// UserFavoriteAddedRequested ...
func (d *Domain) UserFavoriteAddedRequested(ctx context.Context, request service.UserSettingsRequest, sink ports.OutgoingEvents) {
	logger := log.WithFields(log.Fields{
		"function": "Domain.UserFavoriteAddedRequested",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"username": request.Username,
	})
	logger.Trace("starting")
	var response service.UserSettingsRequest
	response.Username = request.Username

	allowed, err := d.defaultAuthZCheck(ctx, request.Session, request.Username)
	if err != nil {
		logger.WithError(err).Error("authZ check failed when adding to favorites setting")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserFavoriteAddError(response)
		return
	}
	if !allowed {
		logger.WithError(err).Info("actor is unauthorized to add favorite")
		d.setErrorInSession(&response.Session, service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError))
		sink.EventUserFavoriteAddError(response)
		return
	}
	err = d.validateSettingsRequest(request)
	if err != nil {
		logger.WithError(err).Info("invalid request to add favorites")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserFavoriteAddError(response)
		return
	}
	err = d.UserStore.UserFavoriteAdd(ctx, request.Username, request.Key, request.Value)
	if err != nil {
		logger.WithError(err).Error("error received when adding favorites")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserFavoriteAddError(response)
		return
	}
	sink.EventUserFavoriteAdded(response)
}

// UserFavoriteDeletedRequested ...
func (d *Domain) UserFavoriteDeletedRequested(ctx context.Context, request service.UserSettingsRequest, sink ports.OutgoingEvents) {
	logger := log.WithFields(log.Fields{
		"function": "Domain.UserFavoriteDeletedRequested",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"username": request.Username,
	})
	logger.Trace("starting")
	var response service.UserSettingsRequest
	response.Username = request.Username

	allowed, err := d.defaultAuthZCheck(ctx, request.Session, request.Username)
	if err != nil {
		logger.WithError(err).Error("authZ check failed when deleting favorites setting")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserFavoriteDeleteError(response)
		return
	}
	if !allowed {
		logger.WithError(err).Info("actor is unauthorized to delete favorite")
		d.setErrorInSession(&response.Session, service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError))
		sink.EventUserFavoriteDeleteError(response)
		return
	}
	err = d.UserStore.UserFavoriteDelete(ctx, request.Username, request.Key, request.Value)
	if err != nil {
		logger.WithError(err).Error("error received when deleting favorites")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserFavoriteDeleteError(response)
		return
	}
	sink.EventUserFavoriteDeleted(response)
}

// UserRecentSetRequested ...
func (d *Domain) UserRecentSetRequested(ctx context.Context, request service.UserSettingsRequest, sink ports.OutgoingEvents) {
	logger := log.WithFields(log.Fields{
		"function": "domain.setUserRecentHandler",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"username": request.Username,
	})
	logger.Trace("starting")
	var response service.UserSettingsRequest
	response.Username = request.Username

	allowed, err := d.defaultAuthZCheck(ctx, request.Session, request.Username)
	if err != nil {
		logger.WithError(err).Error("fail to check if actor is admin to set recent settings")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserRecentSetError(response)
		return
	}
	if !allowed {
		logger.WithError(err).Info("actor is unauthorized to set recents settings")
		d.setErrorInSession(&response.Session, service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError))
		sink.EventUserRecentSetError(response)
		return
	}
	err = d.validateSettingsRequest(request)
	if err != nil {
		logger.WithError(err).Info("invalid request to set recents")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserRecentSetError(response)
		return
	}
	err = d.UserStore.UserRecentSet(ctx, request.Username, request.Key, request.Value)
	if err != nil {
		logger.WithError(err).Error("error received when setting recents")
		d.setErrorInSession(&response.Session, err)
		sink.EventUserRecentSetError(response)
		return
	}
	sink.EventUserRecentSet(response)
}

func (d *Domain) setErrorInSession(session *service.Session, err error) service.CacaoStandardErrorMessage {
	session.ServiceError = service.ToCacaoError(err).GetBase()
	return session.ServiceError.StandardMessage
}

func (d *Domain) validateSettingsRequest(request service.UserSettingsRequest) error {
	if request.Key == "" {
		return service.NewCacaoInvalidParameterErrorWithOptions("settings key cannot be empty")
	}
	if len(request.Key) > types.MaxSettingsKeyLength {
		return service.NewCacaoInvalidParameterErrorWithOptions("settings key is too long")
	}
	if len(request.Value) > types.MaxSettingsValueLength {
		return service.NewCacaoInvalidParameterErrorWithOptions("settings value is too long")
	}
	for _, r := range request.Key {
		if r > unicode.MaxASCII {
			return service.NewCacaoInvalidParameterError("invalid character in settings key")
		}
		if !unicode.In(r, unicode.Letter, unicode.Digit, unicode.Punct) {
			return service.NewCacaoInvalidParameterError("invalid character in settings key")
		}
	}
	for _, r := range request.Value {
		if r > unicode.MaxASCII {
			return service.NewCacaoInvalidParameterError("invalid character in settings value")
		}
		if unicode.IsControl(r) {
			return service.NewCacaoInvalidParameterError("invalid character in settings value")
		}
	}
	return nil
}

// - allow system user to create user
// - actor must exist AND actor must be admin
func (d *Domain) addUserAuthZCheck(ctx context.Context, session service.Session, subjectUsername string) (allowed bool, err error) {
	if session.SessionActor == service.ReservedCacaoSystemActor {
		return true, nil
	}
	if isSpecialUsername(session.SessionActor) {
		// deny all other special actor
		return false, nil
	}
	actorIsAdmin, actorExists, err := d.UserStore.UserIsAdmin(ctx, session.SessionActor)
	if err != nil {
		return false, err
	}
	if !actorExists {
		return false, service.NewCacaoNotFoundError(types.GeneralActorNotFoundError)
	}
	return actorIsAdmin, nil
}

// - actor must exist
// - non-admin can update themselves
// - admin can update everyone
// - return admin status so that the handler can sanitize certain fields
func (d *Domain) updateUserAuthZCheck(ctx context.Context, session service.Session, subjectUsername string) (allowed bool, actorIsAdmin bool, err error) {
	if isSpecialUsername(session.SessionActor) {
		return false, false, nil
	}
	actorIsAdmin, actorExists, err := d.UserStore.UserIsAdmin(ctx, session.SessionActor)
	if err != nil {
		return false, false, err
	}
	if !actorExists {
		return false, false, service.NewCacaoNotFoundError(types.GeneralActorNotFoundError)
	}
	if subjectUsername == session.SessionActor {
		return true, actorIsAdmin, nil
	}
	return actorIsAdmin, actorIsAdmin, nil
}

// actor must exist AND actor must be admin
func (d *Domain) deleteUserAuthZCheck(ctx context.Context, session service.Session, subjectUsername string) (allowed bool, err error) {
	if isSpecialUsername(session.SessionActor) {
		return false, nil
	}
	actorIsAdmin, actorExists, err := d.UserStore.UserIsAdmin(ctx, session.SessionActor)
	if err != nil {
		return false, err
	}
	if !actorExists {
		return false, service.NewCacaoNotFoundError(types.GeneralActorNotFoundError)
	}
	return actorIsAdmin, nil
}
