package domain

import (
	"context"
	"sync"
	"users-microservice/ports"
	"users-microservice/types"

	log "github.com/sirupsen/logrus"
)

// Domain is the base struct for the domain service
type Domain struct {
	QueryIn   ports.IncomingQueryPort
	EventsIn  ports.IncomingEventsPort
	UserStore ports.PersistentStoragePort
}

// NewDomain returns a domain object given the required parameters
func NewDomain(qin ports.IncomingQueryPort, ein ports.IncomingEventsPort, store ports.PersistentStoragePort) *Domain {
	return &Domain{
		QueryIn:   qin,
		EventsIn:  ein,
		UserStore: store,
	}
}

// Init initializes all the specified adapters
func (d *Domain) Init(c types.Specification) error {
	log.WithFields(log.Fields{"function": "domain.Init"}).Debug("starting")

	err := d.UserStore.Init(c)
	if err != nil {
		log.WithError(err).Error("fail to init storage adapter")
		return err
	}

	d.EventsIn.SetHandlers(d)
	err = d.EventsIn.Init(c)
	if err != nil {
		log.WithError(err).Error("fail to init event adapter")
		return err
	}

	err = d.QueryIn.Init(c)
	if err != nil {
		log.WithError(err).Error("fail to init query adapter")
		return err
	}

	return nil
}

// Start will start the domain object, and in turn start all the async adapters
func (d *Domain) Start(parentCtx context.Context) {
	log.WithFields(log.Fields{"function": "domain.Start"}).Debug("starting")
	ctx, cancel := context.WithCancel(parentCtx)
	defer cancel()

	// using WaitGroup to block termination gracefully
	var wg sync.WaitGroup

	d.QueryIn.SetQueryHandlers(d)
	wg.Add(1)
	err := d.QueryIn.Start(ctx, &wg)
	if err != nil {
		log.WithError(err).Error("fail to start query adapter")
		return
	}

	// create a channel for events, eventChan
	wg.Add(1)
	err = d.EventsIn.Start(ctx, &wg)
	if err != nil {
		log.WithError(err).Error("fail to start event adapter")
		return
	}

	wg.Wait()
}
