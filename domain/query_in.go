package domain

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"slices"
	"users-microservice/ports"
	"users-microservice/types"
)

var _ ports.IncomingQueryHandlers = (*Domain)(nil)

// UsersGet ...
// Users can always get their own info; otherwise, must be admin.
func (d *Domain) UsersGet(ctx context.Context, query service.UserModel) service.UserModel {
	logger := log.WithFields(log.Fields{
		"function": "Domain.UsersGet",
		"actor":    query.SessionActor,
		"emulator": query.SessionActor,
		"username": query.Username,
	})
	logger.Trace("starting")

	allowed, err := d.getUserAuthZCheck(ctx, query.Session, query.Username)
	if err != nil {
		logger.WithError(err).Error("fail to check user authorization")
		return service.UserModel{
			Session: service.Session{
				SessionActor:    query.SessionActor,
				SessionEmulator: query.SessionEmulator,
				ServiceError:    service.ToCacaoError(err).GetBase(),
			},
			Username: query.Username,
		}
	}
	if !allowed {
		logger.WithError(err).Errorf("actor not authorized")
		return service.UserModel{
			Session: service.Session{
				SessionActor:    query.SessionActor,
				SessionEmulator: query.SessionEmulator,
				ServiceError:    service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError).GetBase(),
			},
			Username: query.Username,
		}
	}
	fetchedUser, err := d.UserStore.UserGet(ctx, query.Username)
	if err != nil {
		reply := service.UserModel{
			Session: service.Session{
				SessionActor:    query.SessionActor,
				SessionEmulator: query.SessionEmulator,
			},
			Username: query.Username,
		}
		errType := d.setErrorInSession(&reply.Session, err)
		if errType == service.CacaoNotFoundErrorMessage {
			logger.WithError(err).Info("user not found")
		} else {
			logger.WithError(err).Error("fail to get user")
		}
		return reply
	}
	logger.Info("fetched user")
	result := fetchedUser.ConvertToServiceUserModel(query.SessionActor, query.SessionEmulator)
	return *result
}

// UsersGetWithSettings ...
func (d *Domain) UsersGetWithSettings(ctx context.Context, query service.UserModel) service.UserModel {
	user := d.UsersGet(ctx, query)
	if user.ServiceError.StandardError() != "" {
		return user
	}
	var settings common.UserSettings
	err := d.UserStore.UserSettingsGet(ctx, query.Username, &settings)
	if err != nil {
		log.WithField("username", query.Username).WithError(err).Error("fail to fetch user's setting from storage")
		return service.UserModel{
			Session: service.Session{
				SessionActor:    query.SessionActor,
				SessionEmulator: query.SessionEmulator,
				ServiceError:    service.NewCacaoGeneralError(err.Error()).GetBase(),
			},
			Username: query.Username,
		}
	}
	user.UserSettings = settings
	return user
}

// UserSettingsGet ...
func (d *Domain) UserSettingsGet(ctx context.Context, query service.UserSettingsRequest) (service.UserSettingsReply, service.CacaoError) {
	logger := log.WithFields(log.Fields{
		"function": "Domain.UserSettingsGet",
		"actor":    query.SessionActor,
		"emulator": query.SessionActor,
		"username": query.Username,
	})
	logger.Trace("starting")
	allowed, err := d.defaultAuthZCheck(ctx, query.Session, query.Username)
	if err != nil {
		return service.UserSettingsReply{}, service.ToCacaoError(err)
	}
	if !allowed {
		return service.UserSettingsReply{}, service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError)
	}
	var settings common.UserSettings
	if err := d.UserStore.UserSettingsGet(ctx, query.Username, &settings); err != nil {
		logger.WithError(err).Error("fail to fetch settings from storage")
		return service.UserSettingsReply{}, service.ToCacaoError(err)
	}
	logger.Info("fetched user settings")
	return service.UserSettingsReply{UserSettings: settings}, nil
}

// UserConfigsGet ...
func (d *Domain) UserConfigsGet(ctx context.Context, query service.UserSettingsRequest) (service.UserSettingsReply, service.CacaoError) {
	logger := log.WithFields(log.Fields{
		"function": "Domain.UserConfigsGet",
		"actor":    query.SessionActor,
		"emulator": query.SessionActor,
		"username": query.Username,
	})
	logger.Trace("starting")
	allowed, err := d.defaultAuthZCheck(ctx, query.Session, query.Username)
	if err != nil {
		return service.UserSettingsReply{}, service.ToCacaoError(err)
	}
	if !allowed {
		return service.UserSettingsReply{}, service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError)
	}
	var settings common.UserSettings
	if err := d.UserStore.UserSettingsGet(ctx, query.Username, &settings); err != nil {
		logger.WithError(err).Error("fail to fetch configs from storage")
		return service.UserSettingsReply{}, service.ToCacaoError(err)
	}
	logger.Info("fetched user config settings")
	return service.UserSettingsReply{UserSettings: common.UserSettings{Configs: settings.Configs}}, nil
}

// UserFavoritesGet ...
func (d *Domain) UserFavoritesGet(ctx context.Context, query service.UserSettingsRequest) (service.UserSettingsReply, service.CacaoError) {
	logger := log.WithFields(log.Fields{
		"function": "Domain.UserFavoritesGet",
		"actor":    query.SessionActor,
		"emulator": query.SessionActor,
		"username": query.Username,
	})
	logger.Trace("starting")
	allowed, err := d.defaultAuthZCheck(ctx, query.Session, query.Username)
	if err != nil {
		return service.UserSettingsReply{}, service.ToCacaoError(err)
	}
	if !allowed {
		return service.UserSettingsReply{}, service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError)
	}
	var settings common.UserSettings
	if err := d.UserStore.UserSettingsGet(ctx, query.Username, &settings); err != nil {
		logger.WithError(err).Error("fail to fetch favorites from storage")
		return service.UserSettingsReply{}, service.ToCacaoError(err)
	}
	logger.Info("fetched user favorite settings")
	return service.UserSettingsReply{UserSettings: common.UserSettings{Favorites: settings.Favorites}}, nil
}

// UserRecentsGet ...
func (d *Domain) UserRecentsGet(ctx context.Context, query service.UserSettingsRequest) (service.UserSettingsReply, service.CacaoError) {
	logger := log.WithFields(log.Fields{
		"function": "Domain.UserRecentsGet",
		"actor":    query.SessionActor,
		"emulator": query.SessionActor,
		"username": query.Username,
	})
	logger.Trace("starting")
	allowed, err := d.defaultAuthZCheck(ctx, query.Session, query.Username)
	if err != nil {
		return service.UserSettingsReply{}, service.ToCacaoError(err)
	}
	if !allowed {
		return service.UserSettingsReply{}, service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError)
	}
	var settings common.UserSettings
	if err := d.UserStore.UserSettingsGet(ctx, query.Username, &settings); err != nil {
		logger.WithError(err).Error("fail to fetch recents settings from storage")
		return service.UserSettingsReply{}, service.ToCacaoError(err)
	}
	logger.Info("fetched user recent settings")
	return service.UserSettingsReply{UserSettings: common.UserSettings{Recents: settings.Recents}}, nil
}

// UsersList ...
// Any user can always get their own info; otherwise, must be admin
func (d *Domain) UsersList(ctx context.Context, query service.UserListModel) service.UserListModel {
	logger := log.WithFields(log.Fields{
		"function": "Domain.UsersList",
		"actor":    query.SessionActor,
		"emulator": query.SessionActor,
	})
	logger.Debug("starting")

	actorIsAdmin, err := d.listUserAuthZCheck(ctx, query.Session)
	if err != nil {
		logger.WithError(err).Error("fail to check user authorization")
		return service.UserListModel{
			Session: service.Session{
				SessionActor:    query.SessionActor,
				SessionEmulator: query.SessionEmulator,
				ServiceError:    service.ToCacaoError(err).GetBase(),
			},
		}
	}
	if !actorIsAdmin {
		// non-admin user can only list themselves, meaning the list result is just themselves(length of 1)
		fetchedUser, err := d.UserStore.UserGet(ctx, query.SessionActor)
		if err != nil {
			// this should be very rare, and probably result of race condition, since user
			// did exist when we checked its auth just above.
			logger.WithError(err).Warn("user existed, but fetch again failed")
			return service.UserListModel{
				Session: service.Session{
					SessionActor:    query.SessionActor,
					SessionEmulator: query.SessionEmulator,
					ServiceError:    service.NewCacaoGeneralError(err.Error()).GetBase(),
				},
			}
		}
		user := fetchedUser.ConvertToServiceUserModel(query.SessionActor, query.SessionEmulator)
		logger.WithError(err).Info("listed user themself")
		// clear the inner Session(service.UserListModel.Users[].Session), since the info
		// is already in the outer Session (service.UserListModel.Session)
		user.Session = service.Session{}
		return service.UserListModel{
			Session: service.Session{
				SessionActor:    query.SessionActor,
				SessionEmulator: query.SessionEmulator,
			},
			Users: []service.UserModel{*user},
		}
	}

	// make sure that we have a valid maximum count, we don't want to return all the users in one shot.
	if query.Filter.MaxItems <= 0 || query.Filter.MaxItems > types.MaxUserListSize {
		query.Filter.MaxItems = types.MaxUserListSize
	}

	var results []types.DomainUserModel
	results, err = d.UserStore.UserList(ctx, query.Filter)
	if err != nil {
		logger.WithError(err).Error("fail to list user")
		return service.UserListModel{
			Session: service.Session{
				SessionActor:    query.SessionActor,
				SessionEmulator: query.SessionEmulator,
				ServiceError:    service.ToCacaoError(err).GetBase(),
			},
		}
	}
	var convertedList []service.UserModel
	for _, u := range results {
		converted := u.ConvertToServiceUserModel(query.SessionActor, query.SessionEmulator)
		converted.Session = service.Session{}
		convertedList = append(convertedList, *converted)
	}
	logger.WithField("len", len(convertedList)).Info("listed users")
	return service.UserListModel{
		Session: service.Session{
			SessionActor:    query.SessionActor,
			SessionEmulator: query.SessionEmulator,
		},
		Users: convertedList,
	}
}

// - allow system actor to fetch user to check their existence
// - otherwise actor must exist
// - anyone can fetch themselves
// - admin can fetch everyone
func (d *Domain) getUserAuthZCheck(ctx context.Context, session service.Session, subjectUsername string) (allowed bool, err error) {
	if session.SessionActor == service.ReservedCacaoSystemActor {
		return true, nil
	}
	if isSpecialUsername(session.SessionActor) {
		// deny all other special actor
		return false, nil
	}
	actorIsAdmin, actorExists, err := d.UserStore.UserIsAdmin(ctx, session.SessionActor)
	if err != nil {
		return false, err
	}
	if !actorExists {
		return false, service.NewCacaoNotFoundError(types.GeneralActorNotFoundError)
	}
	if subjectUsername == session.SessionActor {
		return true, nil
	}
	return actorIsAdmin, nil
}

// - actor must exist
// - return admin status so that query handler can return different things
func (d *Domain) listUserAuthZCheck(ctx context.Context, session service.Session) (actorIsAdmin bool, err error) {
	if isSpecialUsername(session.SessionActor) {
		return false, service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError)
	}
	var actorExists bool
	actorIsAdmin, actorExists, err = d.UserStore.UserIsAdmin(ctx, session.SessionActor)
	if err != nil {
		return false, err
	}
	if !actorExists {
		return false, service.NewCacaoNotFoundError(types.GeneralActorNotFoundError)
	}
	return actorIsAdmin, nil
}

func (d *Domain) defaultAuthZCheck(ctx context.Context, session service.Session, subjectUsername string) (allowed bool, err error) {
	if isSpecialUsername(session.SessionActor) {
		return false, nil
	}
	actorIsAdmin, actorExists, err := d.UserStore.UserIsAdmin(ctx, session.SessionActor)
	if err != nil {
		return false, err
	}
	if !actorExists {
		return false, service.NewCacaoNotFoundError(types.GeneralActorNotFoundError)
	}
	if session.SessionActor == subjectUsername {
		return true, nil
	}
	if actorIsAdmin {
		return true, nil
	}
	return false, nil
}

// this is checking for special actor for the purpose of authZ checks
func isSpecialUsername(username string) bool {
	return slices.Contains([]string{service.ReservedCacaoSystemActor, service.ReservedAnonymousActor}, username)
}
