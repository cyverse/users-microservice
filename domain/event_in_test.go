package domain

import (
	"context"
	"fmt"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"testing"
	"time"
	portsmocks "users-microservice/ports/mocks"
	"users-microservice/types"
)

func TestDomain_UserAddRequested(t *testing.T) {
	type fields struct {
		UserStore *portsmocks.PersistentStoragePort
	}
	type args struct {
		request service.UserModel
		sink    *portsmocks.OutgoingEvents
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "empty",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "").Return(false, false, nil)
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserAddError", service.UserModel{
						Session: service.Session{
							SessionActor:    "",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoNotFoundError(types.GeneralActorNotFoundError).GetBase(),
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "auth check failed",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "testuser123").Return(false, false, fmt.Errorf("error123"))
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username: "testuser123",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserAddError", service.UserModel{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoGeneralError("error123").GetBase(),
						},
						Username: "testuser123",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "actor not found",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "testuser123").Return(false, false, nil)
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username: "testuser456",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserAddError", service.UserModel{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoNotFoundError(types.GeneralActorNotFoundError).GetBase(),
						},
						Username: "testuser456",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "non-admin create user",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "non-admin_user123").Return(false, true, nil)
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    "non-admin_user123",
						SessionEmulator: "",
					},
					Username:          "testuser123",
					FirstName:         "firstname123",
					LastName:          "lastname123",
					PrimaryEmail:      "",
					IsAdmin:           false,
					DisabledAt:        time.Time{},
					WithSettings:      false,
					UserSettings:      common.UserSettings{},
					CreatedAt:         time.Time{},
					UpdatedAt:         time.Time{},
					UpdatedBy:         "",
					UpdatedEmulatorBy: "",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserAddError", service.UserModel{
						Session: service.Session{
							SessionActor:    "non-admin_user123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError).GetBase(),
						},
						Username: "testuser123",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "admin create user",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "admin_user123").Return(true, true, nil)
					storage.On("UserAdd", context.TODO(), &types.DomainUserModel{
						Username:          "testuser123",
						FirstName:         "firstname123",
						LastName:          "lastname123",
						PrimaryEmail:      "foo@example.com",
						IsAdmin:           false,
						DisabledAt:        time.Time{},
						UserSettings:      types.UserSettings{},
						CreatedAt:         time.Time{},
						UpdatedAt:         time.Time{},
						UpdatedBy:         "",
						UpdatedEmulatorBy: "",
						SessionActor:      "admin_user123",
						SessionEmulator:   "",
						ErrorType:         "",
						ErrorMessage:      "",
					}).Return(nil)
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    "admin_user123",
						SessionEmulator: "",
					},
					Username:          "testuser123",
					FirstName:         "firstname123",
					LastName:          "lastname123",
					PrimaryEmail:      "foo@example.com",
					IsAdmin:           false,
					DisabledAt:        time.Time{},
					WithSettings:      false,
					UserSettings:      common.UserSettings{},
					CreatedAt:         time.Time{},
					UpdatedAt:         time.Time{},
					UpdatedBy:         "",
					UpdatedEmulatorBy: "",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserAdded", service.UserModel{
						Session: service.Session{
							SessionActor:    "admin_user123",
							SessionEmulator: "",
						},
						Username: "testuser123",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "system create user",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserAdd", context.TODO(), &types.DomainUserModel{
						Username:          "testuser123",
						FirstName:         "firstname123",
						LastName:          "lastname123",
						PrimaryEmail:      "foo@example.com",
						IsAdmin:           false,
						DisabledAt:        time.Time{},
						UserSettings:      types.UserSettings{},
						CreatedAt:         time.Time{},
						UpdatedAt:         time.Time{},
						UpdatedBy:         "",
						UpdatedEmulatorBy: "",
						SessionActor:      service.ReservedCacaoSystemActor,
						SessionEmulator:   "",
						ErrorType:         "",
						ErrorMessage:      "",
					}).Return(nil)
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    service.ReservedCacaoSystemActor,
						SessionEmulator: "",
					},
					Username:          "testuser123",
					FirstName:         "firstname123",
					LastName:          "lastname123",
					PrimaryEmail:      "foo@example.com",
					IsAdmin:           false,
					DisabledAt:        time.Time{},
					WithSettings:      false,
					UserSettings:      common.UserSettings{},
					CreatedAt:         time.Time{},
					UpdatedAt:         time.Time{},
					UpdatedBy:         "",
					UpdatedEmulatorBy: "",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserAdded", service.UserModel{
						Session: service.Session{
							SessionActor:    service.ReservedCacaoSystemActor,
							SessionEmulator: "",
						},
						Username: "testuser123",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "system create user with empty username",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    service.ReservedCacaoSystemActor,
						SessionEmulator: "",
					},
					Username:          "",
					FirstName:         "firstname123",
					LastName:          "lastname123",
					PrimaryEmail:      "foo@example.com",
					IsAdmin:           false,
					DisabledAt:        time.Time{},
					WithSettings:      false,
					UserSettings:      common.UserSettings{},
					CreatedAt:         time.Time{},
					UpdatedAt:         time.Time{},
					UpdatedBy:         "",
					UpdatedEmulatorBy: "",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserAddError", service.UserModel{
						Session: service.Session{
							SessionActor:    service.ReservedCacaoSystemActor,
							SessionEmulator: "",
							ServiceError:    service.NewCacaoInvalidParameterError("username not set, cannot add user").GetBase(),
						},
						Username: "",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "system create user with reserved username 1",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    service.ReservedCacaoSystemActor,
						SessionEmulator: "",
					},
					Username:          service.ReservedCacaoSystemActor,
					FirstName:         "firstname123",
					LastName:          "lastname123",
					PrimaryEmail:      "foo@example.com",
					IsAdmin:           false,
					DisabledAt:        time.Time{},
					WithSettings:      false,
					UserSettings:      common.UserSettings{},
					CreatedAt:         time.Time{},
					UpdatedAt:         time.Time{},
					UpdatedBy:         "",
					UpdatedEmulatorBy: "",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserAddError", service.UserModel{
						Session: service.Session{
							SessionActor:    service.ReservedCacaoSystemActor,
							SessionEmulator: "",
							ServiceError:    service.NewCacaoInvalidParameterError(types.UserUsernameReservedCannotAddError).GetBase(),
						},
						Username: service.ReservedCacaoSystemActor,
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "system create user with reserved username 2",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    service.ReservedCacaoSystemActor,
						SessionEmulator: "",
					},
					Username:          service.ReservedAnonymousActor,
					FirstName:         "firstname123",
					LastName:          "lastname123",
					PrimaryEmail:      "foo@example.com",
					IsAdmin:           false,
					DisabledAt:        time.Time{},
					WithSettings:      false,
					UserSettings:      common.UserSettings{},
					CreatedAt:         time.Time{},
					UpdatedAt:         time.Time{},
					UpdatedBy:         "",
					UpdatedEmulatorBy: "",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserAddError", service.UserModel{
						Session: service.Session{
							SessionActor:    service.ReservedCacaoSystemActor,
							SessionEmulator: "",
							ServiceError:    service.NewCacaoInvalidParameterError(types.UserUsernameReservedCannotAddError).GetBase(),
						},
						Username: service.ReservedAnonymousActor,
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "already exists",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserAdd", context.TODO(), &types.DomainUserModel{
						Username:          "testuser123",
						FirstName:         "firstname123",
						LastName:          "lastname123",
						PrimaryEmail:      "foo@example.com",
						IsAdmin:           false,
						DisabledAt:        time.Time{},
						UserSettings:      types.UserSettings{},
						CreatedAt:         time.Time{},
						UpdatedAt:         time.Time{},
						UpdatedBy:         "",
						UpdatedEmulatorBy: "",
						SessionActor:      service.ReservedCacaoSystemActor,
						SessionEmulator:   "",
						ErrorType:         "",
						ErrorMessage:      "",
					}).Return(service.NewCacaoAlreadyExistError(types.UserUsernameExistsCannotAddError))
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    service.ReservedCacaoSystemActor,
						SessionEmulator: "",
					},
					Username:          "testuser123",
					FirstName:         "firstname123",
					LastName:          "lastname123",
					PrimaryEmail:      "foo@example.com",
					IsAdmin:           false,
					DisabledAt:        time.Time{},
					WithSettings:      false,
					UserSettings:      common.UserSettings{},
					CreatedAt:         time.Time{},
					UpdatedAt:         time.Time{},
					UpdatedBy:         "",
					UpdatedEmulatorBy: "",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserAddError", service.UserModel{
						Session: service.Session{
							SessionActor:    service.ReservedCacaoSystemActor,
							SessionEmulator: "",
							ServiceError:    service.NewCacaoAlreadyExistError(types.UserUsernameExistsCannotAddError).GetBase(),
						},
						Username: "testuser123",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "storage errored when add user",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserAdd", context.TODO(), &types.DomainUserModel{
						Username:          "testuser123",
						FirstName:         "firstname123",
						LastName:          "lastname123",
						PrimaryEmail:      "foo@example.com",
						IsAdmin:           false,
						DisabledAt:        time.Time{},
						UserSettings:      types.UserSettings{},
						CreatedAt:         time.Time{},
						UpdatedAt:         time.Time{},
						UpdatedBy:         "",
						UpdatedEmulatorBy: "",
						SessionActor:      service.ReservedCacaoSystemActor,
						SessionEmulator:   "",
						ErrorType:         "",
						ErrorMessage:      "",
					}).Return(fmt.Errorf("error123"))
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    service.ReservedCacaoSystemActor,
						SessionEmulator: "",
					},
					Username:          "testuser123",
					FirstName:         "firstname123",
					LastName:          "lastname123",
					PrimaryEmail:      "foo@example.com",
					IsAdmin:           false,
					DisabledAt:        time.Time{},
					WithSettings:      false,
					UserSettings:      common.UserSettings{},
					CreatedAt:         time.Time{},
					UpdatedAt:         time.Time{},
					UpdatedBy:         "",
					UpdatedEmulatorBy: "",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserAddError", service.UserModel{
						Session: service.Session{
							SessionActor:    service.ReservedCacaoSystemActor,
							SessionEmulator: "",
							ServiceError:    service.NewCacaoGeneralError("error123").GetBase(),
						},
						Username: "testuser123",
					}).Once()
					return out
				}(),
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &Domain{
				UserStore: tt.fields.UserStore,
			}
			d.UserAddRequested(context.TODO(), tt.args.request, tt.args.sink)
			tt.fields.UserStore.AssertExpectations(t)
			tt.args.sink.AssertExpectations(t)
		})
	}
}

func TestDomain_UserUpdateRequested(t *testing.T) {
	type fields struct {
		UserStore *portsmocks.PersistentStoragePort
	}
	type args struct {
		request service.UserModel
		sink    *portsmocks.OutgoingEvents
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "empty",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "").Return(false, false, nil)
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{}, // empty request
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserUpdateError", service.UserModel{
						Session: service.Session{
							SessionActor:    "",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoNotFoundError(types.GeneralActorNotFoundError).GetBase(),
						},
						Username: "",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "auth check failed",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "testuser123").Return(false, false, fmt.Errorf("error123"))
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username: "testuser123",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserUpdateError", service.UserModel{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoGeneralError("error123").GetBase(),
						},
						Username: "testuser123",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "non-admin update self (non-critical fields)",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "testuser123").Return(false, true, nil)
					storage.On("UserGet", context.TODO(), "testuser123").Return(types.DomainUserModel{
						Username:  "testuser123",
						FirstName: "oldFirstName",
					}, nil)
					storage.On("UserUpdate", context.TODO(), &types.DomainUserModel{
						Username:          "testuser123",
						FirstName:         "newFirstName",
						LastName:          "",
						PrimaryEmail:      "",
						IsAdmin:           false,
						DisabledAt:        time.Time{},
						UserSettings:      types.UserSettings{},
						CreatedAt:         time.Time{},
						UpdatedAt:         time.Time{},
						UpdatedBy:         "",
						UpdatedEmulatorBy: "",
						SessionActor:      "testuser123",
						SessionEmulator:   "",
						ErrorType:         "",
						ErrorMessage:      "",
					}).Return(nil)
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username:  "testuser123",
					FirstName: "newFirstName",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserUpdated", service.UserModel{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Username:          "testuser123",
						FirstName:         "",
						LastName:          "",
						PrimaryEmail:      "",
						IsAdmin:           false,
						DisabledAt:        time.Time{},
						WithSettings:      false,
						UserSettings:      common.UserSettings{},
						CreatedAt:         time.Time{},
						UpdatedAt:         time.Time{},
						UpdatedBy:         "testuser123",
						UpdatedEmulatorBy: "",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "non-admin update self (critical fields)",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "testuser123").Return(false, true, nil)
					storage.On("UserGet", context.TODO(), "testuser123").Return(types.DomainUserModel{
						Username:   "testuser123",
						FirstName:  "oldFirstName",
						IsAdmin:    false,
						DisabledAt: time.Time{},
					}, nil)
					storage.On("UserUpdate", context.TODO(), &types.DomainUserModel{
						Username:          "testuser123",
						FirstName:         "",
						LastName:          "",
						PrimaryEmail:      "",
						IsAdmin:           false,       // override by handler
						DisabledAt:        time.Time{}, // override by handler
						UserSettings:      types.UserSettings{},
						CreatedAt:         time.Time{},
						UpdatedAt:         time.Time{},
						UpdatedBy:         "",
						UpdatedEmulatorBy: "",
						SessionActor:      "testuser123",
						SessionEmulator:   "",
						ErrorType:         "",
						ErrorMessage:      "",
					}).Return(nil)
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username:   "testuser123",
					IsAdmin:    true,       // critical field
					DisabledAt: time.Now(), // critical field
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserUpdated", service.UserModel{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Username:          "testuser123",
						FirstName:         "",
						LastName:          "",
						PrimaryEmail:      "",
						IsAdmin:           false,
						DisabledAt:        time.Time{},
						WithSettings:      false,
						UserSettings:      common.UserSettings{},
						CreatedAt:         time.Time{},
						UpdatedAt:         time.Time{},
						UpdatedBy:         "testuser123",
						UpdatedEmulatorBy: "",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "non-admin update other",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "testuser123").Return(false, true, nil)
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username:  "diff-user123",
					FirstName: "newFirstName",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserUpdateError", service.UserModel{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError).GetBase(),
						},
						Username:          "diff-user123",
						FirstName:         "",
						LastName:          "",
						PrimaryEmail:      "",
						IsAdmin:           false,
						DisabledAt:        time.Time{},
						WithSettings:      false,
						UserSettings:      common.UserSettings{},
						CreatedAt:         time.Time{},
						UpdatedAt:         time.Time{},
						UpdatedBy:         "",
						UpdatedEmulatorBy: "",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "admin update other",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "admin-actor123").Return(true, true, nil)
					storage.On("UserGet", context.TODO(), "testuser123").Return(types.DomainUserModel{
						Username:  "testuser123",
						FirstName: "oldFirstName",
					}, nil)
					storage.On("UserUpdate", context.TODO(), &types.DomainUserModel{
						Username:          "testuser123",
						FirstName:         "newFirstName",
						LastName:          "",
						PrimaryEmail:      "",
						IsAdmin:           false,
						DisabledAt:        time.Time{},
						UserSettings:      types.UserSettings{},
						CreatedAt:         time.Time{},
						UpdatedAt:         time.Time{},
						UpdatedBy:         "",
						UpdatedEmulatorBy: "",
						SessionActor:      "admin-actor123",
						SessionEmulator:   "",
						ErrorType:         "",
						ErrorMessage:      "",
					}).Return(nil)
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    "admin-actor123",
						SessionEmulator: "",
					},
					Username:  "testuser123",
					FirstName: "newFirstName",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserUpdated", service.UserModel{
						Session: service.Session{
							SessionActor:    "admin-actor123",
							SessionEmulator: "",
						},
						Username:          "testuser123",
						FirstName:         "",
						LastName:          "",
						PrimaryEmail:      "",
						IsAdmin:           false,
						DisabledAt:        time.Time{},
						WithSettings:      false,
						UserSettings:      common.UserSettings{},
						CreatedAt:         time.Time{},
						UpdatedAt:         time.Time{},
						UpdatedBy:         "admin-actor123",
						UpdatedEmulatorBy: "",
					}).Once()
					return out
				}(),
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &Domain{
				UserStore: tt.fields.UserStore,
			}
			d.UserUpdateRequested(context.TODO(), tt.args.request, tt.args.sink)
			tt.fields.UserStore.AssertExpectations(t)
			tt.args.sink.AssertExpectations(t)
		})
	}
}

func TestDomain_UserDeleteRequested(t *testing.T) {
	type fields struct {
		UserStore *portsmocks.PersistentStoragePort
	}
	type args struct {
		request service.UserModel
		sink    *portsmocks.OutgoingEvents
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "empty",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "").Return(false, false, nil)
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{}, // empty
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserDeleteError", service.UserModel{
						Session: service.Session{
							SessionActor:    "",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoNotFoundError(types.GeneralActorNotFoundError).GetBase(),
						},
						Username: "",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "auth check failed",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "testuser123").Return(false, false, fmt.Errorf("error123"))
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username: "testuser123",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserDeleteError", service.UserModel{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoGeneralError("error123").GetBase(),
						},
						Username: "testuser123",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "non-admin delete self",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "testuser123").Return(false, true, nil)
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username: "testuser123",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserDeleteError", service.UserModel{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError).GetBase(),
						},
						Username: "testuser123",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "non-admin delete other",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "testuser123").Return(false, true, nil)
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username: "diff_user123",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserDeleteError", service.UserModel{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoUnauthorizedError(types.GeneralActorNotAuthorizedError).GetBase(),
						},
						Username: "diff_user123",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "admin delete other",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "admin_user123").Return(true, true, nil)
					storage.On("UserDelete", context.TODO(), "testuser123").Return(nil)
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    "admin_user123",
						SessionEmulator: "",
					},
					Username: "testuser123",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserDeleted", service.UserModel{
						Session: service.Session{
							SessionActor:    "admin_user123",
							SessionEmulator: "",
						},
						Username: "testuser123",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "admin delete self",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "admin_user123").Return(true, true, nil)
					storage.On("UserDelete", context.TODO(), "admin_user123").Return(nil)
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    "admin_user123",
						SessionEmulator: "",
					},
					Username: "admin_user123",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserDeleted", service.UserModel{
						Session: service.Session{
							SessionActor:    "admin_user123",
							SessionEmulator: "",
						},
						Username: "admin_user123",
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "admin delete non-existent-user",
			fields: fields{
				UserStore: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("UserIsAdmin", context.TODO(), "admin_user123").Return(true, true, nil)
					storage.On("UserDelete", context.TODO(), "non-existent-user123").Return(service.NewCacaoNotFoundError("error123"))
					return storage
				}(),
			},
			args: args{
				request: service.UserModel{
					Session: service.Session{
						SessionActor:    "admin_user123",
						SessionEmulator: "",
					},
					Username: "non-existent-user123",
				},
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventUserDeleteError", service.UserModel{
						Session: service.Session{
							SessionActor:    "admin_user123",
							SessionEmulator: "",
							ServiceError:    service.NewCacaoNotFoundError("error123").GetBase(),
						},
						Username: "non-existent-user123",
					}).Once()
					return out
				}(),
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &Domain{
				UserStore: tt.fields.UserStore,
			}
			d.UserDeleteRequested(context.TODO(), tt.args.request, tt.args.sink)
			tt.fields.UserStore.AssertExpectations(t)
			tt.args.sink.AssertExpectations(t)
		})
	}
}
