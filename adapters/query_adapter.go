package adapters

import (
	"context"
	"encoding/json"
	v2 "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"sync"
	"users-microservice/ports"
	"users-microservice/types"

	log "github.com/sirupsen/logrus"

	"gitlab.com/cyverse/cacao-common/service"
)

// QueryAdapter is the adapter implementation for the IncomingQueryPort and OutgoingQueryPort
type QueryAdapter struct {
	nc       *messaging2.NatsConnection
	handlers ports.IncomingQueryHandlers
}

var _ ports.IncomingQueryPort = (*QueryAdapter)(nil)

// Init - connect to NATS
func (q *QueryAdapter) Init(s types.Specification) error {
	logger := log.WithField("function", "QueryAdapter.Init")
	logger.Trace()

	nc, err := types.MessagingConfigFromSpec(s).ConnectNats()
	if err != nil {
		logger.WithError(err).Error("Cannot connect to nats")
		return err
	}
	q.nc = &nc
	logger.Debug("connected to nats")
	return nil
}

// SetQueryHandlers ...
func (q *QueryAdapter) SetQueryHandlers(handlers ports.IncomingQueryHandlers) {
	q.handlers = handlers
}

const queryWorkerCount = 10

// Start tells the adapter to start listening for queries.
func (q *QueryAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	logger := log.WithField("function", "QueryAdapter.Start")
	logger.Trace("start")

	err := q.nc.ListenWithConcurrentWorkers(ctx, map[common.QueryOp]messaging2.QueryHandlerFunc{
		service.NatsSubjectUsersGet:             handlerWrapper(q.handlers.UsersGet),
		service.NatsSubjectUsersGetWithSettings: handlerWrapper(q.handlers.UsersGetWithSettings),
		service.NatsSubjectUserSettingsGet:      handlerWrapperForSettings(q.handlers.UserSettingsGet),
		service.NatsSubjectUserConfigsGet:       handlerWrapperForSettings(q.handlers.UserConfigsGet),
		service.NatsSubjectUserFavoritesGet:     handlerWrapperForSettings(q.handlers.UserFavoritesGet),
		service.NatsSubjectUserRecentsGet:       handlerWrapperForSettings(q.handlers.UserRecentsGet),
		service.NatsSubjectUsersList:            handlerWrapper(q.handlers.UsersList),
	}, wg, types.DefaultQueryChannelBufferSize, queryWorkerCount)
	if err != nil {
		log.WithError(err).Error("fail to start listening for queries on NATS")
		return err
	}
	return nil
}

func handlerWrapper[RequestStructType, ReplyStructType any](handler func(context.Context, RequestStructType) ReplyStructType) messaging2.QueryHandlerFunc {
	return func(ctx context.Context, requestCe v2.Event, writer messaging2.ReplyWriter) {
		var request RequestStructType
		var replyCe v2.Event
		err := json.Unmarshal(requestCe.Data(), &request)
		if err != nil {
			writeMarshalErrorReply(common.QueryOp(requestCe.Type()), err, writer)
			return
		}
		result := handler(ctx, request)
		replyCe, err = messaging2.CreateCloudEvent(result, common.QueryOp(requestCe.Type()), messaging2.AutoPopulateCloudEventSource)
		if err != nil {
			log.WithField("CeType", requestCe.Type()).WithError(err).Error("fail to create cloudevent for query reply")
			return
		}
		err = writer.Write(replyCe)
		if err != nil {
			log.WithField("CeType", requestCe.Type()).WithError(err).Error("fail to write query reply")
			return
		}
	}
}

func handlerWrapperForSettings[RequestStructType, ReplyStructType any](handler func(context.Context, RequestStructType) (ReplyStructType, service.CacaoError)) messaging2.QueryHandlerFunc {
	return func(ctx context.Context, requestCe v2.Event, writer messaging2.ReplyWriter) {
		var request RequestStructType
		err := json.Unmarshal(requestCe.Data(), &request)
		if err != nil {
			writeMarshalErrorReply(common.QueryOp(requestCe.Type()), err, writer)
			return
		}
		var replyCe v2.Event
		result, err1 := handler(ctx, request)
		if err1 != nil {
			replyCe, err = messaging2.CreateCloudEvent(service.UserSettingsReply{Session: service.Session{
				ServiceError: err1.GetBase(),
			}}, common.QueryOp(requestCe.Type()), messaging2.AutoPopulateCloudEventSource)
			if err != nil {
				log.WithField("CeType", requestCe.Type()).WithError(err).Error("fail to create cloudevent for query reply")
				return
			}
		} else {
			replyCe, err = messaging2.CreateCloudEvent(result, common.QueryOp(requestCe.Type()), messaging2.AutoPopulateCloudEventSource)
			if err != nil {
				log.WithField("CeType", requestCe.Type()).WithError(err).Error("fail to create cloudevent for query reply")
				return
			}
		}
		err = writer.Write(replyCe)
		if err != nil {
			log.WithField("CeType", requestCe.Type()).WithError(err).Error("fail to write query reply")
			return
		}
	}
}

// this is used when cannot unmarshal the cloudevent into the request struct.
func writeMarshalErrorReply(op common.QueryOp, err error, writer messaging2.ReplyWriter) {
	reply := service.UserModel{Session: service.Session{ErrorType: service.GeneralUnmarshalFromUserMSError, ErrorMessage: err.Error()}}
	replyCe, err := messaging2.CreateCloudEvent(reply, op, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		log.WithField("CeType", op).WithError(err).Error("fail to create cloudevent for marshal error query reply")
		return
	}
	err = writer.Write(replyCe)
	if err != nil {
		log.WithField("CeType", op).WithError(err).Error("fail to write marshal error query reply")
	}
}

// Close closes the Nats connection
func (q *QueryAdapter) Close() error {
	log.WithField("function", "QueryAdapter.Close").Info("closing")
	return q.nc.Close()
}
