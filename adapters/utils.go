package adapters

import (
	"os"
	"users-microservice/types"

	log "github.com/sirupsen/logrus"
)

// GetCloudEventSourceString gets the pod name from the environment variable; otherwise, just reports an error and returns an empty string
func GetCloudEventSourceString() string {
	log.WithField("function", "GetCloudEventSourceString").Trace("start")
	pname := os.Getenv(types.EnvPodNameVar)
	if pname == "" {
		log.WithField("function", "GetCloudEventSourceString").Warn("?Could not obtain the pod name")
	}
	return types.CloudEventSourceBaseValue + pname
}
