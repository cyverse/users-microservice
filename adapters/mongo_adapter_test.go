package adapters

import (
	"context"
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"go.mongodb.org/mongo-driver/bson"
	"os"
	"sync"
	"testing"
	"time"
	"users-microservice/ports"
	"users-microservice/types"
)

var exampleUser = types.DomainUserModel{
	Username:     "testuser123@cyverse.org",
	FirstName:    "first123",
	LastName:     "last123",
	PrimaryEmail: "example@cyverse.org",
	IsAdmin:      true,
	DisabledAt:   time.Time{},
	UserSettings: types.UserSettings{
		Configs: map[string]string{
			"config1": "config1-1",
		},
		Favorites: map[string][]string{
			"fav1": {"fav1-1", "fav1-2"},
		},
		Recents: map[string]string{
			"recent1": "recent1-1",
		},
	},
	CreatedAt:         time.Time{},
	UpdatedAt:         time.Time{},
	UpdatedBy:         "",
	UpdatedEmulatorBy: "",
	SessionActor:      "actor123",
	SessionEmulator:   "",
	ErrorType:         "",
	ErrorMessage:      "",
}

const storageTimeout = time.Minute * 5

func TestMongoAdapter(t *testing.T) {
	if skipTestIfNoMongoDB(t) {
		return
	}
	newStorage := func() ports.PersistentStoragePort {
		var s types.Specification
		err := envconfig.Process(types.ConfigVarPrefix, &s)
		if err != nil {
			t.Fatal(err)
		}
		s.AppContext = context.Background()
		storage := &MongoAdapter{}
		err = storage.Init(s)
		if err != nil {
			t.Fatal(err)
		}
		return storage
	}
	cleanupStorage := func(storage ports.PersistentStoragePort) {
		adapter := storage.(*MongoAdapter)
		err := adapter.collection.Drop(context.Background())
		if err != nil {
			t.Fatal(err)
		}
		err = adapter.settings.Drop(context.Background())
		if err != nil {
			t.Fatal(err)
		}
		err = adapter.connection.Disconnect()
		if err != nil {
			t.Fatal(err)
		}
	}
	t.Run("fetch non-existing user", func(t *testing.T) {
		storage := newStorage()
		defer cleanupStorage(storage)
		ctx, cancel := context.WithTimeout(context.Background(), storageTimeout)
		defer cancel()

		_, err := storage.UserGet(ctx, "not-exist")
		assert.Error(t, err)
		assert.Equal(t, service.CacaoNotFoundErrorMessage, service.ToCacaoError(err).StandardError())
		assert.Equal(t, types.UserUsernameNotFoundError, service.ToCacaoError(err).ContextualError())
	})
	t.Run("check exist false", func(t *testing.T) {
		storage := newStorage()
		defer cleanupStorage(storage)
		ctx, cancel := context.WithTimeout(context.Background(), storageTimeout)
		defer cancel()

		exists, err := storage.UserExists(ctx, "not-exist")
		if !assert.NoError(t, err) {
			return
		}
		assert.False(t, exists)
	})
	t.Run("add, fetch, delete, fetch", func(t *testing.T) {
		storage := newStorage()
		defer cleanupStorage(storage)
		ctx, cancel := context.WithTimeout(context.Background(), storageTimeout)
		defer cancel()

		err := storage.UserAdd(ctx, &types.DomainUserModel{
			Username:     "testuser123@cyverse.org",
			FirstName:    "first123",
			LastName:     "last123",
			PrimaryEmail: "example@cyverse.org",
			IsAdmin:      true,
			DisabledAt:   time.Time{},
			UserSettings: types.UserSettings{
				Configs: map[string]string{
					"config1": "config1-1",
				},
				Favorites: map[string][]string{
					"fav1": {"fav1-1", "fav1-2"},
				},
				Recents: map[string]string{
					"recent1": "recent1-1",
				},
			},
			CreatedAt:         time.Time{},
			UpdatedAt:         time.Time{},
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
			SessionActor:      "actor123",
			SessionEmulator:   "",
			ErrorType:         "",
			ErrorMessage:      "",
		})
		if !assert.NoError(t, err) {
			return
		}

		var fetchUser types.DomainUserModel
		fetchUser, err = storage.UserGet(ctx, "testuser123@cyverse.org")
		if !assert.NoError(t, err) {
			return
		}

		// check timestamp separately
		assert.Greater(t, time.Since(fetchUser.CreatedAt), time.Duration(0))
		assert.Greater(t, time.Since(fetchUser.UpdatedAt), time.Duration(0))
		assert.Equal(t, fetchUser.UpdatedAt, fetchUser.CreatedAt)

		fetchUser.CreatedAt = time.Time{} // erase the timestamp for easy comparison with assert
		fetchUser.UpdatedAt = time.Time{} // erase the timestamp for easy comparison with assert

		assert.Equal(t, types.DomainUserModel{
			Username:          "testuser123@cyverse.org",
			FirstName:         "first123",
			LastName:          "last123",
			PrimaryEmail:      "example@cyverse.org",
			IsAdmin:           true,
			DisabledAt:        time.Time{},
			UserSettings:      types.UserSettings{},
			CreatedAt:         time.Time{},
			UpdatedAt:         time.Time{},
			UpdatedBy:         "actor123",
			UpdatedEmulatorBy: "",
			SessionActor:      "",
			SessionEmulator:   "",
			ErrorType:         "",
			ErrorMessage:      "",
		}, fetchUser)

		err = storage.UserDelete(ctx, "testuser123@cyverse.org")
		if !assert.NoError(t, err) {
			return
		}

		_, err = storage.UserGet(ctx, "testuser123@cyverse.org")
		assert.Error(t, err)
		assert.Equal(t, service.CacaoNotFoundErrorMessage, service.ToCacaoError(err).StandardError())
		assert.Equal(t, types.UserUsernameNotFoundError, service.ToCacaoError(err).ContextualError())

		exists, err := storage.UserExists(ctx, "testuser123@cyverse.org")
		if !assert.NoError(t, err) {
			return
		}
		assert.False(t, exists)
	})
	t.Run("add N, list", func(t *testing.T) {
		storage := newStorage()
		defer cleanupStorage(storage)
		ctx, cancel := context.WithTimeout(context.Background(), storageTimeout)
		defer cancel()

		const N = 100

		var usernameList = make([]string, 0)

		for i := 0; i < N; i++ {
			username := fmt.Sprintf("testuser-%d", i)
			usernameList = append(usernameList, username)

			err := storage.UserAdd(ctx, &types.DomainUserModel{
				Username:     username,
				FirstName:    "first123",
				LastName:     "last123",
				PrimaryEmail: "example@cyverse.org",
				IsAdmin:      true,
				DisabledAt:   time.Time{},
				UserSettings: types.UserSettings{
					Configs: map[string]string{
						"config1": "config1-1",
					},
					Favorites: map[string][]string{
						"fav1": {"fav1-1", "fav1-2"},
					},
					Recents: map[string]string{
						"recent1": "recent1-1",
					},
				},
				CreatedAt:         time.Time{},
				UpdatedAt:         time.Time{},
				UpdatedBy:         "",
				UpdatedEmulatorBy: "",
				SessionActor:      "actor123",
				SessionEmulator:   "",
				ErrorType:         "",
				ErrorMessage:      "",
			})
			if !assert.NoErrorf(t, err, username) {
				return
			}
		}
		list, err := storage.UserList(ctx, service.UserListFilter{
			Field:           "",
			Value:           "",
			SortBy:          0,
			MaxItems:        0,
			Start:           0,
			IncludeDisabled: false,
		})
		if !assert.NoError(t, err) {
			return
		}

		assert.NotNil(t, list)
		assert.Len(t, list, N)

		for _, user := range list {
			assert.Contains(t, usernameList, user.Username)
			// check timestamp separately
			assert.Greater(t, time.Since(user.CreatedAt), time.Duration(0))
			assert.Greater(t, time.Since(user.UpdatedAt), time.Duration(0))

			user.CreatedAt = time.Time{} // erase the timestamp for easy comparison with assert
			user.UpdatedAt = time.Time{} // erase the timestamp for easy comparison with assert
			assert.Equal(t, types.DomainUserModel{
				Username:          user.Username,
				FirstName:         "first123",
				LastName:          "last123",
				PrimaryEmail:      "example@cyverse.org",
				IsAdmin:           true,
				DisabledAt:        time.Time{},
				UserSettings:      types.UserSettings{},
				CreatedAt:         time.Time{},
				UpdatedAt:         time.Time{},
				UpdatedBy:         "actor123",
				UpdatedEmulatorBy: "",
				SessionActor:      "",
				SessionEmulator:   "",
				ErrorType:         "",
				ErrorMessage:      "",
			}, user)
		}
	})
	t.Run("add, re-add", func(t *testing.T) {
		storage := newStorage()
		defer cleanupStorage(storage)
		ctx, cancel := context.WithTimeout(context.Background(), storageTimeout)
		defer cancel()

		user := exampleUser
		err := storage.UserAdd(ctx, &user)
		if !assert.NoError(t, err) {
			return
		}

		// re-add
		err = storage.UserAdd(ctx, &user)
		if !assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoAlreadyExistErrorMessage, service.ToCacaoError(err).StandardError())
		assert.Equal(t, types.UserUsernameExistsCannotAddError, service.ToCacaoError(err).ContextualError())
	})
	t.Run("add check-admin=true", func(t *testing.T) {
		storage := newStorage()
		defer cleanupStorage(storage)
		ctx, cancel := context.WithTimeout(context.Background(), storageTimeout)
		defer cancel()

		err := storage.UserAdd(ctx, &types.DomainUserModel{
			Username:     "testuser123@cyverse.org",
			FirstName:    "first123",
			LastName:     "last123",
			PrimaryEmail: "example@cyverse.org",
			IsAdmin:      true,
		})
		if !assert.NoError(t, err) {
			return
		}

		isAdmin, exists, err := storage.UserIsAdmin(ctx, "testuser123@cyverse.org")
		if !assert.NoError(t, err) {
			return
		}
		assert.True(t, isAdmin)
		assert.True(t, exists)

		err = storage.UserUpdate(ctx, &types.DomainUserModel{
			Username:     "testuser123@cyverse.org",
			FirstName:    "first123",
			LastName:     "last123",
			PrimaryEmail: "example@cyverse.org",
			IsAdmin:      false, // IsAdmin set to false
		})
		if !assert.NoError(t, err) {
			return
		}

		// check again after update
		isAdmin, exists, err = storage.UserIsAdmin(ctx, "testuser123@cyverse.org")
		if !assert.NoError(t, err) {
			return
		}
		assert.False(t, isAdmin)
		assert.True(t, exists)
	})
	t.Run("add check-admin=false", func(t *testing.T) {
		storage := newStorage()
		defer cleanupStorage(storage)
		ctx, cancel := context.WithTimeout(context.Background(), storageTimeout)
		defer cancel()

		err := storage.UserAdd(ctx, &types.DomainUserModel{
			Username:     "testuser123@cyverse.org",
			FirstName:    "first123",
			LastName:     "last123",
			PrimaryEmail: "example@cyverse.org",
			IsAdmin:      false,
		})
		if !assert.NoError(t, err) {
			return
		}

		isAdmin, exists, err := storage.UserIsAdmin(ctx, "testuser123@cyverse.org")
		if !assert.NoError(t, err) {
			return
		}
		assert.False(t, isAdmin)
		assert.True(t, exists)
	})
	t.Run("empty list", func(t *testing.T) {
		storage := newStorage()
		defer cleanupStorage(storage)
		ctx, cancel := context.WithTimeout(context.Background(), storageTimeout)
		defer cancel()

		list, err := storage.UserList(ctx, service.UserListFilter{
			Field:           "",
			Value:           "",
			SortBy:          0,
			MaxItems:        0,
			Start:           0,
			IncludeDisabled: false,
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Len(t, list, 0)
		assert.Nil(t, list)
	})
	t.Run("delete non-existent", func(t *testing.T) {
		storage := newStorage()
		defer cleanupStorage(storage)
		ctx, cancel := context.WithTimeout(context.Background(), storageTimeout)
		defer cancel()

		err := storage.UserDelete(ctx, "non-existent")
		if assert.Error(t, err) {
			return
		}
		assert.Equal(t, service.CacaoNotFoundErrorMessage, service.ToCacaoError(err).StandardError())
		assert.Equal(t, types.UserUsernameNotFoundError, service.ToCacaoError(err).ContextualError())
	})
	t.Run("update non-existent", func(t *testing.T) {
		storage := newStorage()
		defer cleanupStorage(storage)
		ctx, cancel := context.WithTimeout(context.Background(), storageTimeout)
		defer cancel()

		err := storage.UserUpdate(ctx, &types.DomainUserModel{
			Username:  "not-exist",
			FirstName: "foo",
		})
		if assert.NoError(t, err) {
			return
		}
	})
	t.Run("update email", func(t *testing.T) {
		storage := newStorage()
		defer cleanupStorage(storage)
		ctx, cancel := context.WithTimeout(context.Background(), storageTimeout)
		defer cancel()

		user := exampleUser
		err := storage.UserAdd(ctx, &user)
		if assert.NoError(t, err) {
			return
		}

		var fetchUser1 types.DomainUserModel
		fetchUser1, err = storage.UserGet(ctx, user.Username)
		if assert.NoError(t, err) {
			return
		}
		assert.NotEqual(t, "email-has-been-updated@cyverse.org", fetchUser1.PrimaryEmail)

		err = storage.UserUpdate(ctx, &types.DomainUserModel{
			Username:     user.Username,
			PrimaryEmail: "email-has-been-updated@cyverse.org",
		})
		if assert.NoError(t, err) {
			return
		}

		var fetchUser2 types.DomainUserModel
		fetchUser2, err = storage.UserGet(ctx, user.Username)
		if assert.NoError(t, err) {
			return
		}
		assert.Equal(t, "email-has-been-updated@cyverse.org", fetchUser2.PrimaryEmail)
		assert.NotEqual(t, fetchUser2.CreatedAt, fetchUser2.UpdatedAt)
		assert.Greater(t, fetchUser2.UpdatedAt.Sub(fetchUser2.CreatedAt), time.Duration(0))

		// check if the rest of the user object has changed
		fetchUser1.CreatedAt = time.Time{}
		fetchUser1.UpdatedAt = time.Time{}
		fetchUser2.CreatedAt = time.Time{}
		fetchUser2.UpdatedAt = time.Time{}
		fetchUser1.PrimaryEmail = ""
		fetchUser2.PrimaryEmail = ""
		assert.Equal(t, fetchUser1, fetchUser2)
	})
	t.Run("concurrent fetch-or-create", func(t *testing.T) {
		storage := newStorage()
		defer cleanupStorage(storage)
		ctx, cancel := context.WithTimeout(context.Background(), storageTimeout)
		defer cancel()

		const N = 100
		var wg sync.WaitGroup
		wg.Add(N)

		for i := 0; i < N; i++ {
			go func() {
				defer wg.Done()

				time.Sleep(2 * time.Millisecond)

				_, err := storage.UserGet(ctx, exampleUser.Username)
				// err should be either not-found or nil
				if err == nil {
					return
				}
				if err != nil {
					if !assert.Equal(t, service.CacaoNotFoundErrorMessage, service.ToCacaoError(err).StandardError()) {
						return
					}
					if !assert.Equal(t, types.UserUsernameNotFoundError, service.ToCacaoError(err).ContextualError()) {
						return
					}
				}

				user := exampleUser
				err = storage.UserAdd(ctx, &user)
				// err should be either already-exists or nil
				if err != nil {
					if !assert.Equal(t, service.CacaoAlreadyExistErrorMessage, service.ToCacaoError(err).StandardError()) {
						return
					}
					if !assert.Equal(t, types.UserUsernameExistsCannotAddError, service.ToCacaoError(err).ContextualError()) {
						return
					}
				}
			}()
		}
		wg.Wait()
		list, err := storage.UserList(ctx, service.UserListFilter{})
		if !assert.NoError(t, err) {
			return
		}
		assert.Len(t, list, 1)
	})
	t.Run("set config setting", func(t *testing.T) {
		storage := newStorage()
		defer cleanupStorage(storage)
		ctx, cancel := context.WithTimeout(context.Background(), storageTimeout)
		defer cancel()

		err := storage.UserConfigSet(ctx, "testuser123@cyverse.org", "Foo", "Bar")
		if !assert.NoError(t, err) {
			return
		}
		var settings common.UserSettings
		err = storage.UserSettingsGet(ctx, "testuser123@cyverse.org", &settings)
		if !assert.NoError(t, err) {
			return
		}
		if !assert.Len(t, settings.Configs, 1) {
			return
		}
		assert.Equal(t, "Bar", settings.Configs["Foo"])
	})
	t.Run("add/delete favorite setting", func(t *testing.T) {
		storage := newStorage()
		defer cleanupStorage(storage)
		ctx, cancel := context.WithTimeout(context.Background(), storageTimeout)
		defer cancel()

		err := storage.UserFavoriteAdd(ctx, "testuser123@cyverse.org", "Foo", "Bar")
		if !assert.NoError(t, err) {
			return
		}
		var settings common.UserSettings
		err = storage.UserSettingsGet(ctx, "testuser123@cyverse.org", &settings)
		if !assert.NoError(t, err) {
			return
		}
		if !assert.Len(t, settings.Favorites, 1) {
			return
		}
		assert.ElementsMatch(t, []string{"Bar"}, settings.Favorites["Foo"])
		err = storage.UserFavoriteAdd(ctx, "testuser123@cyverse.org", "Foo", "Baz")
		if !assert.NoError(t, err) {
			return
		}
		err = storage.UserSettingsGet(ctx, "testuser123@cyverse.org", &settings)
		if !assert.NoError(t, err) {
			return
		}
		if !assert.Len(t, settings.Favorites, 1) {
			return
		}
		assert.ElementsMatch(t, []string{"Bar", "Baz"}, settings.Favorites["Foo"])

		err = storage.UserFavoriteDelete(ctx, "testuser123@cyverse.org", "Foo", "Bar")
		if !assert.NoError(t, err) {
			return
		}
		err = storage.UserSettingsGet(ctx, "testuser123@cyverse.org", &settings)
		if !assert.NoError(t, err) {
			return
		}
		if !assert.Len(t, settings.Favorites, 1) {
			return
		}
		assert.Equal(t, []string{"Baz"}, settings.Favorites["Foo"])

		err = storage.UserFavoriteDelete(ctx, "testuser123@cyverse.org", "Foo", "Baz")
		if !assert.NoError(t, err) {
			return
		}
		err = storage.UserSettingsGet(ctx, "testuser123@cyverse.org", &settings)
		if !assert.NoError(t, err) {
			return
		}
		if !assert.Len(t, settings.Favorites, 1) {
			return
		}
		assert.Equal(t, []string{}, settings.Favorites["Foo"])
	})
	t.Run("set recent setting", func(t *testing.T) {
		storage := newStorage()
		defer cleanupStorage(storage)
		ctx, cancel := context.WithTimeout(context.Background(), storageTimeout)
		defer cancel()

		err := storage.UserRecentSet(ctx, "testuser123@cyverse.org", "Foo", "Bar")
		if !assert.NoError(t, err) {
			return
		}
		var settings common.UserSettings
		err = storage.UserSettingsGet(ctx, "testuser123@cyverse.org", &settings)
		if !assert.NoError(t, err) {
			return
		}
		if !assert.Len(t, settings.Recents, 1) {
			return
		}
		assert.Equal(t, "Bar", settings.Recents["Foo"])
	})
}

func skipTestIfNoMongoDB(t *testing.T) bool {
	val, ok := os.LookupEnv("CI_INTEGRATION_TEST_MONGO")
	if !ok {
		t.Skip("CI_INTEGRATION_TEST_MONGO not set")
		return true
	}
	if val != "true" {
		t.Skip("CI_INTEGRATION_TEST_MONGO not set")
		return true
	}
	return false
}

// This test is to ensure that the bson tag for types.DomainUserModel.IsAdmin
// field is always "is_admin", this is important since we create index for this
// field.
func Test_IsAdminField(t *testing.T) {
	adminUser := types.DomainUserModel{Username: "testuser123", IsAdmin: true}
	marshalAdmin, err := bson.Marshal(adminUser)
	if !assert.NoError(t, err) {
		return
	}
	var adminMap bson.M
	if err = bson.Unmarshal(marshalAdmin, &adminMap); !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, true, adminMap["is_admin"])

	nonAdminUser := types.DomainUserModel{Username: "testuser123", IsAdmin: false}
	marshalNonAdmin, err := bson.Marshal(nonAdminUser)
	if !assert.NoError(t, err) {
		return
	}
	var nonAdminMap bson.M
	if err = bson.Unmarshal(marshalNonAdmin, &nonAdminMap); !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, false, nonAdminMap["is_admin"])
}
