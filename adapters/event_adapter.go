package adapters

import (
	"context"
	"encoding/json"
	"github.com/cloudevents/sdk-go/v2"
	"sync"
	"users-microservice/ports"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"users-microservice/types"
)

// EventAdapter adapter to the IncomingEventsPort and the OutgoingEventsPort
type EventAdapter struct {
	startCtx context.Context // set in Start()

	conn     messaging2.EventConnection
	handlers ports.IncomingEventHandlers
}

// NewEventAdapter ...
func NewEventAdapter(conn messaging2.EventConnection) *EventAdapter {
	return &EventAdapter{
		conn: conn,
	}
}

// Init connects to the events queue and subscribe to it
func (ea *EventAdapter) Init(s types.Specification) error {
	logger := log.WithFields(log.Fields{
		"function": "EventAdapter.Init",
	})
	logger.Trace()

	return nil
}

// InitChannel initializes the channel with the domain object
func (ea *EventAdapter) InitChannel(uc chan types.UserOp, sc chan types.UserSettingsOp) {
	log.WithFields(log.Fields{
		"function": "EventAdapter.InitChannel",
	}).Trace()
}

// SetHandlers ...
func (ea *EventAdapter) SetHandlers(handlers ports.IncomingEventHandlers) {
	ea.handlers = handlers
}

// Start tells the adapter to start listening for updates. In this case the NATS subscriber would
// listen to a message and then call doAction whenever a message is received.
func (ea *EventAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{
		"function": "EventAdapter.Start",
	})
	logger.Trace("start")
	ea.startCtx = ctx

	err := ea.conn.Listen(ctx, map[common.EventType]messaging2.EventHandlerFunc{
		service.EventUserAddRequested:            eventHandlerWrapper(ea.handlers.UserAddRequested),
		service.EventUserUpdateRequested:         eventHandlerWrapper(ea.handlers.UserUpdateRequested),
		service.EventUserDeleteRequested:         eventHandlerWrapper(ea.handlers.UserDeleteRequested),
		service.EventUserConfigSetRequested:      eventHandlerWrapper(ea.handlers.UserConfigSetRequested),
		service.EventUserFavoriteAddRequested:    eventHandlerWrapper(ea.handlers.UserFavoriteAddedRequested),
		service.EventUserFavoriteDeleteRequested: eventHandlerWrapper(ea.handlers.UserFavoriteDeletedRequested),
		service.EventUserRecentSetRequested:      eventHandlerWrapper(ea.handlers.UserRecentSetRequested),
	}, wg)
	if err != nil {
		return err
	}
	return nil
}

func eventHandlerWrapper[T any](handler func(context.Context, T, ports.OutgoingEvents)) messaging2.EventHandlerFunc {
	return func(ctx context.Context, event v2.Event, writer messaging2.EventResponseWriter) error {
		var request T
		err := json.Unmarshal(event.Data(), &request)
		if err != nil {
			return err
		}
		newCtx, cancel := context.WithTimeout(ctx, messaging2.DefaultStanEventsTimeout)
		handler(newCtx, request, EventOut{messaging2.GetTransactionID(&event), writer})
		cancel()
		return nil
	}
}

// Close closes the STAN connection
func (ea *EventAdapter) Close() error {
	return nil
}

// EventOut implements ports.OutgoingEvents. This is used to wrap
// messaging2.EventResponseWriter into a ports.OutgoingEvents. And exposed to
// handlers (ports.IncomingEventHandlers) as parameter.
type EventOut struct {
	tid    common.TransactionID
	writer messaging2.EventResponseWriter
}

var _ ports.OutgoingEvents = (*EventOut)(nil)

// EventUserAdded ...
func (e EventOut) EventUserAdded(model service.UserModel) {
	e.write(model, service.EventUserAdded)
}

// EventUserAddError ...
func (e EventOut) EventUserAddError(model service.UserModel) {
	e.write(model, service.EventUserAddError)
}

// EventUserUpdated ...
func (e EventOut) EventUserUpdated(model service.UserModel) {
	e.write(model, service.EventUserUpdated)
}

// EventUserUpdateError ...
func (e EventOut) EventUserUpdateError(model service.UserModel) {
	e.write(model, service.EventUserUpdateError)
}

// EventUserDeleted ...
func (e EventOut) EventUserDeleted(model service.UserModel) {
	e.write(model, service.EventUserDeleted)
}

// EventUserDeleteError ...
func (e EventOut) EventUserDeleteError(model service.UserModel) {
	e.write(model, service.EventUserDeleteError)
}

// EventUserConfigSet ...
func (e EventOut) EventUserConfigSet(event service.UserSettingsRequest) {
	e.write(event, service.EventUserConfigSet)
}

// EventUserConfigSetError ...
func (e EventOut) EventUserConfigSetError(event service.UserSettingsRequest) {
	e.write(event, service.EventUserConfigSetError)
}

// EventUserFavoriteAdded ...
func (e EventOut) EventUserFavoriteAdded(event service.UserSettingsRequest) {
	e.write(event, service.EventUserFavoriteAdded)
}

// EventUserFavoriteAddError ...
func (e EventOut) EventUserFavoriteAddError(event service.UserSettingsRequest) {
	e.write(event, service.EventUserFavoriteAddError)
}

// EventUserFavoriteDeleted ...
func (e EventOut) EventUserFavoriteDeleted(event service.UserSettingsRequest) {
	e.write(event, service.EventUserFavoriteDeleted)
}

// EventUserFavoriteDeleteError ...
func (e EventOut) EventUserFavoriteDeleteError(event service.UserSettingsRequest) {
	e.write(event, service.EventUserFavoriteDeleteError)
}

// EventUserRecentSet ...
func (e EventOut) EventUserRecentSet(event service.UserSettingsRequest) {
	e.write(event, service.EventUserRecentSet)
}

// EventUserRecentSetError ...
func (e EventOut) EventUserRecentSetError(event service.UserSettingsRequest) {
	e.write(event, service.EventUserRecentSetError)
}

func (e EventOut) write(data interface{}, eventType common.EventType) {
	ce, err := messaging2.CreateCloudEventWithTransactionID(data, eventType, messaging2.AutoPopulateCloudEventSource, e.tid)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"tid":    e.tid,
			"ceType": eventType,
		}).Error("fail to create cloudevent")
		return
	}
	err = e.writer.Write(ce)
	if err != nil {
		log.WithFields(log.Fields{
			"tid":    e.tid,
			"ceType": ce.Type(),
		}).WithError(err).Error("fail to write cloudevent as response event")
		return
	}
}
