package adapters

import (
	"context"
	"fmt"
	"gitlab.com/cyverse/cacao-common/common"
	"strconv"
	"time"
	"users-microservice/types"

	log "github.com/sirupsen/logrus"

	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/service"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// MongoAdapter is the adapter implementation to PersistentStoragePort but for Mongo
type MongoAdapter struct {
	connection *db.MongoDBConnection
	collection *mongo.Collection
	settings   *mongo.Collection
}

const defaultMongoTimeout = time.Second * 5

// Init - establish the connection to Mongodb and create the collection
func (m *MongoAdapter) Init(s types.Specification) error {

	mconfig := db.MongoDBConfig{
		URL:    s.DbURI,
		DBName: s.DbName}

	var err error
	m.connection, err = db.NewMongoDBConnection(&mconfig)
	if err != nil {
		return err
	}

	err = m.connection.Client.Ping(s.AppContext, nil)
	if err != nil {
		return err
	}

	m.collection = m.connection.Client.Database(s.DbName).Collection(types.MongoDbUserCollection)
	m.settings = m.connection.Client.Database(s.DbName).Collection(types.MongoDbUserSettingsCollection)

	ctx, cancelFunc := context.WithTimeout(s.AppContext, time.Second*10)
	defer cancelFunc()
	m.createIndex(ctx)
	err = m.setValidationSchemas(ctx)
	if err != nil {
		log.WithError(err).Error("fail to set validation schema")
		return err
	}
	err = m.checkReservedUsernames(ctx)
	if err != nil {
		log.WithError(err).Error("there are documents in Mongo that make use of the reserved username")
		return err
	}

	return nil
}

func (m *MongoAdapter) createIndex(ctx context.Context) {
	// this index is so that MongoAdapter.UserIsAdmin() will be a covered query.
	indexName, err := m.collection.Indexes().CreateOne(ctx, mongo.IndexModel{
		Keys:    bson.M{"is_admin": 1},
		Options: nil,
	})
	if err != nil {
		log.WithError(err).Error("fail to create index for is_admin")
		return
	}
	log.WithField("indexName", indexName).Info("created index for is_admin")
}

// return error is there is documents in DB that use reserved username, this check is just in case some reserved username is accidentally created.
func (m *MongoAdapter) checkReservedUsernames(ctx context.Context) error {
	filter := bson.M{
		"_id": bson.M{
			"$in": types.ReservedUsernameList,
		},
	}
	documentCount, err := m.collection.CountDocuments(ctx, filter)
	if err != nil {
		return err
	}
	if documentCount > 0 {
		return fmt.Errorf("there are user with reserved username in the users collection")
	}
	documentCount, err = m.settings.CountDocuments(context.Background(), filter)
	if err != nil {
		return err
	}
	if documentCount > 0 {
		return fmt.Errorf("there are user with reserved username in the settings collection")
	}
	return nil
}

var userCollectionSchema = bson.M{
	"$jsonSchema": bson.M{
		"bsonType":             "object",
		"required":             bson.A{"_id", "primary_email", "is_admin", "created_at"},
		"additionalProperties": false,
		"properties": bson.M{
			"_id": bson.M{
				"bsonType":    "string",
				"description": "username",
				"minLength":   int32(1),
				"maxLength":   int32(types.MaxUsernameLength),
			},
			"first_name": bson.M{
				"bsonType":  "string",
				"maxLength": int32(types.MaxFirstLastNameLength),
			},
			"last_name": bson.M{
				"bsonType":  "string",
				"maxLength": int32(types.MaxFirstLastNameLength),
			},
			"primary_email": bson.M{
				"bsonType":  "string",
				"maxLength": int32(types.MaxEmailLength),
			},
			"is_admin": bson.M{
				"bsonType":    "bool",
				"description": "whether the user is admin or not",
			},
			"disabled_at": bson.M{
				"bsonType":    bson.A{"date", "null"},
				"description": "timestamp of when user is disabled, use null or golang-zero-value to denote active(not disabled) user",
			},
			"created_at": bson.M{
				"bsonType":    "date",
				"description": "timestamp of when user is created at",
			},
			"updated_at": bson.M{
				"bsonType":    "date",
				"description": "timestamp of when user is last updated at",
			},
			"updated_by": bson.M{
				"bsonType":    "string",
				"description": "the actor(username) who last update the user, empty string means user is never updated",
				"maxLength":   int32(types.MaxUsernameLength),
			},
			"updated_emulator_by": bson.M{
				"bsonType":    "string",
				"description": "the emulator(username) who last update the user, empty string means last update has no emulator or user is never updated",
				"maxLength":   int32(types.MaxUsernameLength),
			},
		},
	},
}

var userSettingsCollectionSchema = bson.M{
	"$jsonSchema": bson.M{
		"bsonType":             "object",
		"required":             bson.A{"_id"},
		"additionalProperties": false,
		"properties": bson.M{
			"_id": bson.M{
				"bsonType":    "string",
				"description": "username",
				"minLength":   int32(1),
				"maxLength":   int32(types.MaxUsernameLength),
			},
			"configs": bson.M{
				"bsonType":      "object",
				"maxProperties": int32(types.MaxSettingsConfigPairs),
			},
			"favorites": bson.M{
				"bsonType":      "object",
				"maxProperties": int32(types.MaxSettingsFavoritePairs),
				"patternProperties": bson.M{
					"^.*$": bson.M{
						"bsonType": "array",
						"items": bson.M{
							"bsonType": "string",
						},
						"maxItems":    int32(types.MaxFavoriteItemsPerKey),
						"uniqueItems": true,
					},
				},
			},
			"recents": bson.M{
				"bsonType":      "object",
				"maxProperties": int32(types.MaxSettingsRecentPairs),
			},
		},
	},
}

func (m *MongoAdapter) setValidationSchemas(ctx context.Context) error {
	err := db.CreateCollectionWithSchema(ctx, m.connection, types.MongoDbUserCollection, userCollectionSchema)
	if err != nil {
		return err
	}
	err = db.CreateCollectionWithSchema(ctx, m.connection, types.MongoDbUserSettingsCollection, userSettingsCollectionSchema)
	if err != nil {
		return err
	}
	return nil
}

// UserAdd - add a new user to the database
// This method will automatically update the CreatedAt and UpdatedAt
func (m *MongoAdapter) UserAdd(ctx context.Context, user *types.DomainUserModel) error {
	logger := log.WithFields(log.Fields{
		"function": "MongoAdapter.UserAdd",
		"username": user.Username,
	})
	logger.Trace("start")
	if _, ok := ctx.Deadline(); !ok {
		var cancelFunc context.CancelFunc
		ctx, cancelFunc = context.WithTimeout(ctx, defaultMongoTimeout)
		defer cancelFunc()
	}

	// let's add values to the created field names
	user.CreatedAt = time.Now()
	user.UpdatedAt = user.CreatedAt
	user.UpdatedBy = user.SessionActor

	_, err := m.settings.InsertOne(ctx, userSettings{
		Username:  user.Username,
		Configs:   map[string]string{},
		Favorites: map[string][]string{},
		Recents:   map[string]string{},
	})
	if err != nil {
		if !db.IsDuplicateError(err) {
			return err
		}
	}

	_, err = m.collection.InsertOne(ctx, user)
	if err != nil {
		if db.IsDocumentValidationFailure(err) {
			return service.NewCacaoInvalidParameterError("user does not conform to schema")
		}
		if db.IsDuplicateError(err) {
			// since "_id" is the only unique field currently, a duplicated key error means username already exists.
			return service.NewCacaoAlreadyExistError(types.UserUsernameExistsCannotAddError)
		}
		logger.WithError(err).Error("fail to insert user")
		return err
	}

	return nil
}

// UserDelete - delete a user from the database
func (m *MongoAdapter) UserDelete(ctx context.Context, username string) error {
	logger := log.WithFields(log.Fields{
		"function": "MongoAdapter.UserDelete",
		"username": username,
	})
	logger.Trace("start")
	if _, ok := ctx.Deadline(); !ok {
		var cancelFunc context.CancelFunc
		ctx, cancelFunc = context.WithTimeout(ctx, defaultMongoTimeout)
		defer cancelFunc()
	}

	filter := bson.M{"_id": username}
	resp, err := m.collection.DeleteOne(ctx, filter)
	if err != nil {
		logger.WithError(err).Error("fail to delete user")
		return err
	} else if resp.DeletedCount == 0 {
		return service.NewCacaoNotFoundError(types.UserUsernameNotFoundError)
	}
	return nil
}

// UserGet - return a user and associated data from the database
func (m *MongoAdapter) UserGet(ctx context.Context, username string) (user types.DomainUserModel, err error) {
	logger := log.WithFields(log.Fields{
		"function": "MongoAdapter.UserGet",
		"username": username,
	})
	logger.Trace("start")
	if _, ok := ctx.Deadline(); !ok {
		var cancelFunc context.CancelFunc
		ctx, cancelFunc = context.WithTimeout(ctx, defaultMongoTimeout)
		defer cancelFunc()
	}

	filter := bson.M{"_id": username}
	r := m.collection.FindOne(ctx, filter)
	if db.IsNoDocumentError(r.Err()) {
		logger.Trace("not found")
		return user, service.NewCacaoNotFoundError(types.UserUsernameNotFoundError)
	}
	if err = r.Decode(&user); err != nil {
		logger.WithError(err).Error("could not decode user")
		return user, err
	}
	return user, nil
}

// UserExists ...
func (m *MongoAdapter) UserExists(ctx context.Context, username string) (exists bool, err error) {
	logger := log.WithFields(log.Fields{
		"function": "MongoAdapter.UserGet",
		"username": username,
	})
	logger.Trace("start")
	if _, ok := ctx.Deadline(); !ok {
		var cancelFunc context.CancelFunc
		ctx, cancelFunc = context.WithTimeout(ctx, defaultMongoTimeout)
		defer cancelFunc()
	}

	filter := bson.M{"_id": username}
	// empty project, so only _id is returned, therefore this is always a covered query.
	r := m.collection.FindOne(ctx, filter, options.FindOne().SetProjection(bson.M{}))
	if db.IsNoDocumentError(r.Err()) {
		logger.Trace("not found")
		return false, nil
	}
	var staging struct {
		Username string `bson:"_id"`
	}
	if err = r.Decode(&staging); err != nil {
		logger.WithError(err).Error("could not decode user")
		return false, err
	}
	return true, nil
}

// UserIsAdmin ...
func (m *MongoAdapter) UserIsAdmin(ctx context.Context, username string) (isAdmin bool, exists bool, err error) {
	logger := log.WithFields(log.Fields{
		"function": "MongoAdapter.UserGet",
		"username": username,
	})
	logger.Trace("start")
	if _, ok := ctx.Deadline(); !ok {
		var cancelFunc context.CancelFunc
		ctx, cancelFunc = context.WithTimeout(ctx, defaultMongoTimeout)
		defer cancelFunc()
	}

	filter := bson.M{"_id": username}
	r := m.collection.FindOne(ctx, filter, options.FindOne().SetProjection(bson.M{"is_admin": 1})) // setup index so that this is a covered query.
	if db.IsNoDocumentError(r.Err()) {
		logger.Trace("not found")
		return false, false, nil
	}
	var staging struct {
		Username string `bson:"_id"`
		IsAdmin  bool   `bson:"is_admin"`
	}
	if err = r.Decode(&staging); err != nil {
		logger.WithError(err).Error("could not decode user")
		return false, false, err
	}
	return staging.IsAdmin, true, nil
}

// UserList returns a list of the users in the database
func (m *MongoAdapter) UserList(ctx context.Context, filter service.UserListFilter) ([]types.DomainUserModel, error) {
	logger := log.WithFields(log.Fields{
		"function": "MongoAdapter.UserList",
		"field":    filter.Field,
		"value":    filter.Value,
		"sort":     filter.SortBy,
		"start":    filter.Start,
		"maxItems": filter.MaxItems,
	})
	logger.Trace("start")
	if _, ok := ctx.Deadline(); !ok {
		var cancelFunc context.CancelFunc
		ctx, cancelFunc = context.WithTimeout(ctx, defaultMongoTimeout)
		defer cancelFunc()
	}

	filterField := bson.M{} // default is to return all
	switch filter.Field {
	case service.IsAdminFilterField:
		boolval, _ := strconv.ParseBool(filter.Value)
		filterField = bson.M{"is_admin": boolval}
	default:

	}

	// default sort is by
	logger.Trace("constructing options")
	opts := options.Find()
	if filter.SortBy == service.AscendingSort {
		opts.SetSort(bson.D{primitive.E{Key: "_id", Value: 1}})
	} else {
		opts.SetSort(bson.D{primitive.E{Key: "_id", Value: -1}})
	}
	opts.SetLimit(int64(filter.MaxItems))
	opts.SetSkip(int64(filter.Start))

	logger.Trace("executing find")
	cur, err := m.collection.Find(ctx, filterField, opts)
	if err != nil {
		logger.WithError(err).Error("fail to list users in DB")
		return nil, err
	}
	defer cur.Close(ctx)

	var dusers []types.DomainUserModel
	err = cur.All(ctx, &dusers)
	if err != nil {
		logger.WithError(err).Error("fail to decode list of users")
		return nil, err
	}

	return dusers, nil
}

// UserUpdate - update the properties for a user in the database
// This method will automatically update the UpdatedAt
func (m *MongoAdapter) UserUpdate(ctx context.Context, user *types.DomainUserModel) error {
	logger := log.WithFields(log.Fields{
		"function": "MongoAdapter.UserUpdate",
		"username": user.Username,
	})
	logger.Trace("start")
	if _, ok := ctx.Deadline(); !ok {
		var cancelFunc context.CancelFunc
		ctx, cancelFunc = context.WithTimeout(ctx, defaultMongoTimeout)
		defer cancelFunc()
	}

	// opts := options.Update().SetUpsert(false)
	filter := bson.M{"_id": user.Username}

	user.UpdatedAt = time.Now()
	user.UpdatedBy = user.SessionActor

	_, err := m.collection.ReplaceOne(ctx, filter, user)
	if err != nil {
		if db.IsDocumentValidationFailure(err) {
			return service.NewCacaoInvalidParameterError("user does not conform to schema")
		}
		logger.WithError(err).Error("fail to update user in DB")
		return err
	}
	return nil
}

// storage schema of the user settings in the settings collection
type userSettings struct {
	Username  string              `bson:"_id"`
	Configs   map[string]string   `bson:"configs"`
	Favorites map[string][]string `bson:"favorites"`
	Recents   map[string]string   `bson:"recents"`
}

// UserSettingsGet ...
func (m *MongoAdapter) UserSettingsGet(ctx context.Context, username string, settings *common.UserSettings) error {
	logger := log.WithFields(log.Fields{
		"function": "MongoAdapter.UserSettingsGet",
		"username": username,
	})
	logger.Trace("start")
	if _, ok := ctx.Deadline(); !ok {
		var cancelFunc context.CancelFunc
		ctx, cancelFunc = context.WithTimeout(ctx, defaultMongoTimeout)
		defer cancelFunc()
	}

	filter := bson.M{"_id": username}
	r := m.settings.FindOne(ctx, filter)
	if db.IsNoDocumentError(r.Err()) {
		return nil // return empty settings
	}
	var result userSettings
	if err := r.Decode(&result); err != nil {
		logger.WithError(err).Error("could not decode user settings")
		return err
	}
	settings.Configs = result.Configs
	settings.Favorites = result.Favorites
	settings.Recents = result.Recents
	return nil
}

// UserConfigSet ...
func (m *MongoAdapter) UserConfigSet(ctx context.Context, username string, key string, value string) error {
	logger := log.WithFields(log.Fields{
		"function": "MongoAdapter.UserConfigSet",
		"username": username,
		"key":      key,
		"value":    value,
	})
	logger.Trace("start")
	if _, ok := ctx.Deadline(); !ok {
		var cancelFunc context.CancelFunc
		ctx, cancelFunc = context.WithTimeout(ctx, defaultMongoTimeout)
		defer cancelFunc()
	}

	filter := bson.M{"_id": username}
	opts := options.Update().SetUpsert(true)
	update := bson.M{"$set": bson.M{"configs." + key: value}}
	_, err := m.settings.UpdateOne(ctx, filter, update, opts)
	if err != nil {
		if db.IsDocumentValidationFailure(err) {
			return service.NewCacaoInvalidParameterError("user config settings does not conform to schema")
		}
		logger.WithError(err).Error("fail to update user configs in DB")
		return err
	}
	return nil
}

// UserFavoriteAdd ...
func (m *MongoAdapter) UserFavoriteAdd(ctx context.Context, username string, key string, value string) error {
	logger := log.WithFields(log.Fields{
		"function": "MongoAdapter.UserAdd",
		"username": username,
		"key":      key,
		"value":    value,
	})
	logger.Trace("start")
	if _, ok := ctx.Deadline(); !ok {
		var cancelFunc context.CancelFunc
		ctx, cancelFunc = context.WithTimeout(ctx, defaultMongoTimeout)
		defer cancelFunc()
	}

	filter := bson.M{"_id": username}
	update := bson.M{"$push": bson.M{"favorites." + key: value}}
	opts := options.Update().SetUpsert(true)
	updateOneResult, err := m.settings.UpdateOne(ctx, filter, update, opts)
	if err != nil {
		if db.IsDocumentValidationFailure(err) {
			return service.NewCacaoInvalidParameterError("user favorite settings does not conform to schema")
		}
		logger.WithError(err).Error("fail to add user's favorites in DB")
		return err
	}
	log.Trace(updateOneResult)
	return nil
}

// UserFavoriteDelete ...
func (m *MongoAdapter) UserFavoriteDelete(ctx context.Context, username string, key string, value string) error {
	logger := log.WithFields(log.Fields{
		"function": "MongoAdapter.UserFavoriteDelete",
		"username": username,
		"key":      key,
		"value":    value,
	})
	logger.Trace("start")
	if _, ok := ctx.Deadline(); !ok {
		var cancelFunc context.CancelFunc
		ctx, cancelFunc = context.WithTimeout(ctx, defaultMongoTimeout)
		defer cancelFunc()
	}

	filter := bson.M{"_id": username}
	update := bson.M{"$pull": bson.M{"favorites." + key: value}}
	_, err := m.settings.UpdateOne(ctx, filter, update, nil)
	if err != nil {
		logger.WithError(err).Error("fail to delete user's favorites in DB")
		return err
	}
	return nil
}

// UserRecentSet ...
func (m *MongoAdapter) UserRecentSet(ctx context.Context, username string, key string, value string) error {
	logger := log.WithFields(log.Fields{
		"function": "MongoAdapter.UserRecentSet",
		"username": username,
		"key":      key,
		"value":    value,
	})
	logger.Trace("start")
	if _, ok := ctx.Deadline(); !ok {
		var cancelFunc context.CancelFunc
		ctx, cancelFunc = context.WithTimeout(ctx, defaultMongoTimeout)
		defer cancelFunc()
	}

	filter := bson.M{"_id": username}
	opts := options.Update().SetUpsert(true)
	update := bson.M{"$set": bson.M{"recents." + key: value}}
	_, err := m.settings.UpdateOne(ctx, filter, update, opts)
	if err != nil {
		if db.IsDocumentValidationFailure(err) {
			return service.NewCacaoInvalidParameterError("user recent settings does not conform to schema")
		}
		logger.WithError(err).Error("fail to update user's recent settings in DB")
		return err
	}
	return nil
}

// Close ...
func (m *MongoAdapter) Close() error {
	log.WithField("function", "MongoAdapter.Close").Info("closing")
	if m.connection != nil {
		return m.connection.Disconnect()
	}
	return nil
}
